<?php

namespace App\Repositories\Frontend\Reports;

use App\Exceptions\GeneralException;
use App\Models\Reportcategory\Reportcategory;
use App\Models\ReportMapCategory\ReportMapCategory;
use App\Models\Reportsubcategory\Reportsubcategory;
use App\Models\ReportMapSubcategory\ReportMapSubcategory;
use App\Models\Region\Region;
use App\Models\ReportMapRegion\ReportMapRegion;
use App\Models\ReportTags\ReportTag;
use App\Models\ReportMapTags\ReportMapTags;
use App\Models\ReportDetails\ReportDetails;
use App\Models\ReportMapRreportDetails\ReportMapRreportDetails;
use App\Models\Report\Report;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class ReportsRepository.
 */
class ReportfrontRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Report::class;

    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct() {
        $this->upload_path = 'img' . DIRECTORY_SEPARATOR . 'report' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('do');
    }

    /**
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->leftjoin(config('access.users_table'), config('access.users_table') . '.id', '=', config('module.reports.table') . '.created_by')
                        ->select([
                            config('module.reports.table') . '.id',
                            config('module.reports.table') . '.name',
                            config('module.reports.table') . '.status',
                            config('module.reports.table') . '.created_by',
                            config('module.reports.table') . '.created_at',
                            config('access.users_table') . '.first_name as user_name',
        ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input) {
        $reportDetails = array();

        $tagsArray = $this->createTags($input['tags']);
        $categoriesArray = $this->createCategories($input['categories']);
        $subcategoriesArray = $this->createsubCategories($input['subcategories']);
        $regionArray = $this->createRegion($input['region']);


        unset($input['tags'], $input['categories'], $input['region'], $input['subcategories']);

        $reportDetails = $input;

        unset($reportDetails["name"], $reportDetails["featured_image"], $reportDetails["slug"], $reportDetails["status"],$reportDetails["pages"],$reportDetails["single_price"], $reportDetails["multi_price"], $reportDetails["ent_price"], $reportDetails["type"]);

        unset($input["meta_description"], $input["meta_keywords"], $input["cannonical_link"], $input["meta_title"], $input["publish_datetime"], $input["toc"],  $input["list_tbl_fig"]);

        $rdArray = $this->createReportdetails($reportDetails);
        DB::transaction(function () use ($input, $tagsArray, $categoriesArray, $subcategoriesArray, $regionArray, $rdArray) {
            $input['slug'] = str_slug($input['name']);
            $input = $this->uploadImage($input);
            $input['created_by'] = access()->user()->id;
            $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
            if ($report = Report::create($input)) {
                // Inserting associated category's id in mapper table
                if (count($categoriesArray)) {
                    $report->categories()->sync($categoriesArray);
                }

                // Inserting associated regionArray's id in mapper table
                if (count($regionArray)) {
                    $report->Region()->sync($regionArray);
                }


                // Inserting associated tag's id in mapper table
                if (count($tagsArray)) {
                    $report->tags()->sync($tagsArray);
                }

                // Inserting associated subcategory's id in mapper table
                if (count($subcategoriesArray)) {
                    $report->subcategories()->sync($subcategoriesArray);
                }


                // Inserting associated subcategory's id in mapper table
                if (count($rdArray)) {
                    $report->reportdetails()->sync($rdArray);
                }

//                event(new ReportCreated($report));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.reports.create_error'));
        });
    }

    /**
     * Update Report.
     *
     * @param \App\Models\Reports\Report $report
     * @param array                  $input
     */
    public function update(Report $report, array $input) {
        $reportDetails = array();

        $tagsArray = $this->createTags($input['tags']);
        $categoriesArray = $this->createCategories($input['categories']);
        $subcategoriesArray = $this->createsubCategories($input['subcategories']);
        $regionArray = $this->createRegion($input['region']);


        unset($input['tags'], $input['categories'], $input['region'], $input['subcategories']);
        $reportDetails = $input;
        unset($reportDetails["name"], $reportDetails["featured_image"], $reportDetails["slug"],$reportDetails["pages"],$reportDetails["single_price"], $reportDetails["multi_price"], $reportDetails["ent_price"],$reportDetails["type"],$reportDetails["status"]);

        unset($input["meta_description"], $input["meta_keywords"], $input["cannonical_link"], $input["meta_title"], $input["publish_datetime"], $input["pages"], $input["toc"],  $input["list_tbl_fig"]);
        $rdArray = $this->updateReportdetails($report, $reportDetails);
        // Uploading Image
        if (array_key_exists('featured_image', $input)) {
            $this->deleteOldFile($report);
            $input = $this->uploadImage($input);
        }

        DB::transaction(function () use ($report, $input, $tagsArray, $categoriesArray, $subcategoriesArray, $regionArray, $rdArray) {

            $input['slug'] = str_slug($input['name']);
            $input['updated_by'] = access()->user()->id;
            $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);

            if ($report->update($input)) {

                // Inserting associated category's id in mapper table
                if (count($categoriesArray)) {
                    $report->categories()->sync($categoriesArray);
                }

                // Inserting associated regionArray's id in mapper table
                if (count($regionArray)) {
                    $report->Region()->sync($regionArray);
                }


                // Inserting associated tag's id in mapper table
                if (count($tagsArray)) {
                    $report->tags()->sync($tagsArray);
                }

                // Inserting associated subcategory's id in mapper table
                if (count($subcategoriesArray)) {
                    $report->subcategories()->sync($subcategoriesArray);
                }

                // Inserting associated subcategory's id in mapper table
                if (count($rdArray)) {
                    $report->reportdetails()->sync($rdArray);
                }
//                event(new ReportUpdated($report));

                return true;
            }

            throw new GeneralException(
            trans('exceptions.backend.reports.update_error')
            );
        });
    }

    /**
     * Creating Tags.
     *
     * @param array $tags
     *
     * @return array
     */
    public function createTags($tags) {
        //Creating a new array for tags (newly created)
        $tags_array = [];

        foreach ($tags as $tag) {
            if (is_numeric($tag)) {
                $tags_array[] = $tag;
            } else {
                $newTag = ReportTag::create(['name' => $tag, 'status' => 1, 'created_by' => 1]);
                $tags_array[] = $newTag->id;
            }
        }

        return $tags_array;
    }

    /**
     * Creating Categories.
     *
     * @param Array($createsubCategories)
     *
     * @return array
     */
    public function createsubCategories($createsubCategories) {
        //Creating a new array for categories (newly created)
        $categories_array = [];

        foreach ($createsubCategories as $subcategory) {
            if (is_numeric($subcategory)) {
                $categories_array[] = $subcategory;
            } else {
                $newCategory = Reportsubcategory::create(['name' => $subcategory, 'status' => 1, 'created_by' => 1]);

                $categories_array[] = $newCategory->id;
            }
        }

        return $categories_array;
    }

    /**
     * Creating $region.
     *
     * @param Array($region)
     *
     * @return array
     */
    public function createRegion($regions) {
        //Creating a new array for categories (newly created)
        $region_array = [];

        foreach ($regions as $region) {
            if (is_numeric($region)) {
                $region_array[] = $region;
            } else {
                $newregion = Region::create(['name' => $region, 'status' => 1, 'created_by' => 1]);

                $region_array[] = $newregion->id;
            }
        }

        return $region_array;
    }

    /**
     * Creating Categories.
     *
     * @param Array($categories)
     *
     * @return array
     */
    public function createCategories($categories) {
        //Creating a new array for categories (newly created)
        $categories_array = [];

        foreach ($categories as $category) {
            if (is_numeric($category)) {
                $categories_array[] = $category;
            } else {
                $newCategory = ReportCategory::create(['name' => $category, 'status' => 1, 'created_by' => 1]);

                $categories_array[] = $newCategory->id;
            }
        }

        return $categories_array;
    }

    /**
     * @param \App\Models\Reports\Report $report
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Report $report) {
        DB::transaction(function () use ($report) {
            if ($report->delete()) {
                ReportMapCategory::where('report_id', $report->id)->delete();
                ReportMapSubcategory::where('report_id', $report->id)->delete();
                ReportMapTags::where('report_id', $report->id)->delete();
                ReportMapRegion::where('report_id', $report->id)->delete();
                ReportMapRreportDetails::where('report_id', $report->id)->delete();
                $reportDetails = ReportDetails::find($report->id);
                $reportDetails->delete();
//                event(new ReportDeleted($report));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.reports.delete_error'));
        });
    }

    /**
     * Creating $region.
     *
     * @param Array($region)
     *
     * @return array
     */
    public function createReportdetails($reportdetails) {
        //Creating a new array for categories (newly created)

        $rd_array = [];
        $reportdetails['publish_datetime'] = Carbon::parse($reportdetails['publish_datetime']);
        $reportdetails['created_by'] = access()->user()->id;
        if ($reportdetails = ReportDetails::create($reportdetails)) {
            $rd_array[] = $reportdetails->id;
        }
        return $rd_array;
    }

    /**
     * Creating $region.
     *
     * @param Array($region)
     *
     * @return array
     */
    public function updateReportdetails($report, $rarray) {
        $rdata = $report->reportDetails->toArray();
        $rdetailsId = $rdata[0]["id"];
        if ($rdetailsId) {
            $rd_array = [];
            $reportDetails = ReportDetails::find($rdetailsId);
            $rarray['publish_datetime'] = Carbon::parse($rarray['publish_datetime']);
            $rarray['updated_by'] = access()->user()->id;
            $reportdetails = $reportDetails->update($rarray);
            if ($reportdetails) {
                $rd_array[] = $rdetailsId;
            }
            return $rd_array;
        }
    }

    /**
     * Upload Image.
     *
     * @param array $input
     *
     * @return array $input
     */
    public function uploadImage($input) {
        $avatar = $input['featured_image'];

        if (isset($input['featured_image']) && !empty($input['featured_image'])) {
            $fileName = time() . $avatar->getClientOriginalName();

            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()),'public');

            $input = array_merge($input, ['featured_image' => $fileName]);

            return $input;
        }
    }

    /**
     * Destroy Old Image.
     *
     * @param int $id
     */
    public function deleteOldFile($model) {
        $fileName = $model->featured_image;

        return $this->storage->delete($this->upload_path . $fileName);
    }

}
