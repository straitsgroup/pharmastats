<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use DB;

/**
 * Class GeoGraphicalRepository.
 */
class GeoGraphicalRepository extends BaseRepository {

    /**
     * 
     * @param - $regionId
     * @return - json
     * @description - returns countries with their cities
     */
    public function getCountriesCitiesByRegion($regionId) {
        try {
            $regions = DB::table("regions")
                    ->select("region_id", "name", "type", "parent")
                    ->where('parent', $regionId)
                    ->orWhere('super_parent', $regionId)
                    ->get()
                    ->toArray();

            if (!empty($regions) && isset($regions)) {
                $countriesCitiesArr = [];
                foreach ($regions as $key => $value) {
                    $parent = $value->parent;
                    $regionId = $value->region_id;
                    if ($value->type == 2) {
                        $countriesCitiesArr[$regionId] = $value;
                    } else if ($value->type == 3) {
                        $countriesCitiesArr[$parent]->cities[$regionId] = $value;
                    }
                }
            }
            return json_encode($countriesCitiesArr);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

}
