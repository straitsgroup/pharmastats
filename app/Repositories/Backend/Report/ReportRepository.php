<?php

namespace App\Repositories\Backend\Reports;

use App\Events\Backend\Reports\ReportCreated;
use App\Events\Backend\Reports\ReportDeleted;
use App\Events\Backend\Reports\ReportUpdated;
use App\Exceptions\GeneralException;
use App\Models\Reportcategory\Reportcategory;
use App\Models\ReportMapCategory\ReportMapCategory;
use App\Models\Reportsubcategory\Reportsubcategory;
use App\Models\ReportMapSubcategory\ReportMapSubcategory;
use App\Models\Region\Region;
use App\Models\ReportMapRegion\ReportMapRegion;
use App\Models\ReportTags\ReportTag;
use App\Models\ReportMapTags\ReportMapTags;
use App\Models\ReportDetails\ReportDetails;
use App\Models\ReportMapRreportDetails\ReportMapRreportDetails;
use App\Models\Report\Report;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Storage;
use App\Models\CustomReport\CustomReport;

/**
 * Class ReportsRepository.
 */
class ReportsRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Report::class;

    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct() {
        $this->upload_path = 'img' . DIRECTORY_SEPARATOR . 'report' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('do');
    }

    /**
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->leftjoin(config('access.users_table'), config('access.users_table') . '.id', '=', config('module.reports.table') . '.created_by')
                        ->select([
                            config('module.reports.table') . '.id',
                            config('module.reports.table') . '.name',
                            config('module.reports.table') . '.status',
                            config('module.reports.table') . '.created_by',
                            config('module.reports.table') . '.created_at',
                            config('module.reports.table') . '.slug',
                            config('access.users_table') . '.first_name as user_name',
        ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input) {

        if ($this->query()->where('name', $input['name'])->whereNull('deleted_at')->first()) {
            return false;
//            throw new GeneralException(trans('exceptions.backend.report.already_exists'));
        }

        $reportDetails = array();

        $tagsArray = $this->createTags($input['tags']);
        $categoriesArray = $this->createCategories($input['categories']);
        $subcategoriesArray = $this->createsubCategories($input['subcategories']);
        $regionArray = $this->createRegion($input['region']);


        unset($input['tags'], $input['categories'], $input['region'], $input['subcategories']);

        $reportDetails = $input;

        unset($reportDetails["name"], $reportDetails["featured_image"], $reportDetails["slug"], $reportDetails["status"], $reportDetails["pages"], $reportDetails["single_price"], $reportDetails["multi_price"], $reportDetails["ent_price"], $reportDetails['publish_datetime'], $reportDetails['report_type']);
        unset($reportDetails["cagr"], $reportDetails["revenue"], $reportDetails["key_highlight_image"]);

        unset($input["meta_description"], $input["meta_keywords"], $input["dc_description"], $input["cannonical_link"], $input["meta_title"], $input["toc"], $input["list_tbl_fig"]);

        $rdArray = $this->createReportdetails($reportDetails);
        return DB::transaction(function () use ($input, $tagsArray, $categoriesArray, $subcategoriesArray, $regionArray, $rdArray, $reportDetails) {
                    $input['slug'] = str_slug($reportDetails['meta_keywords']);
                    if (array_key_exists('featured_image', $input)) {
                        $input = $this->uploadImage($input);
                    }
                    if (array_key_exists('key_highlight_image', $input)) {
                        $input = $this->uploadImage($input);
                    }
                    $input['created_by'] = access()->user()->id;
                    $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);

                    if ($report = Report::create($input)) {
                        // Inserting associated category's id in mapper table
                        if (count($categoriesArray)) {
                            $report->categories()->sync($categoriesArray);
                        }

                        // Inserting associated regionArray's id in mapper table
                        if (count($regionArray)) {
                            $report->Region()->sync($regionArray);
                        }


                        // Inserting associated tag's id in mapper table
                        if (count($tagsArray)) {
                            $report->tags()->sync($tagsArray);
                        }

                        // Inserting associated subcategory's id in mapper table
                        if (count($subcategoriesArray)) {
                            $report->subcategories()->sync($subcategoriesArray);
                        }


                        // Inserting associated subcategory's id in mapper table
                        if (count($rdArray)) {
                            $report->reportdetails()->sync($rdArray);
                        }

                        foreach ($categoriesArray as $key => $cat) {
                            $notificationArr = [];
                            $notificationArr['report_id'] = $report->id;
                            $notificationArr['report_cat_id'] = $cat;
                            $notificationArr['type'] = 1;
                            $notificationArr['msg'] = "New Report available..! " . $input['name'];
                            $notificationArr['status'] = 1;
                            $notificationArr['primary_id'] = $report->id;
                            $notificationArr['primary_table'] = "reports";

                            \DB::table("report_notifications")->insert($notificationArr);
                        }

//                event(new ReportCreated($report));

                        return true;
                    }

                    throw new GeneralException(trans('exceptions.backend.reports.create_error'));
                });
    }

    /**
     * Update Report.
     *
     * @param \App\Models\Reports\Report $report
     * @param array                  $input
     */
    public function update(Report $report, array $input) {
        if ($this->query()->where('name', $input['name'])->where('id', '!=', $report->id)->first()) {
            return false;
//            throw new GeneralException(trans('exceptions.backend.report.already_exists'));
        }
        $reportDetails = array();
        $tagsArray = $this->createTags($input['tags']);
        $categoriesArray = $this->createCategories($input['categories']);
        $subcategoriesArray = $this->createsubCategories($input['subcategories']);
        $regionArray = $this->createRegion($input['region']);


        unset($input['tags'], $input['categories'], $input['region'], $input['subcategories']);
        $reportDetails = $input;
        unset($reportDetails["name"], $reportDetails["featured_image"], $reportDetails["slug"], $reportDetails["pages"], $reportDetails["single_price"], $reportDetails["multi_price"], $reportDetails["ent_price"], $reportDetails["status"], $reportDetails['publish_datetime'], $reportDetails['report_type']);
        unset($reportDetails["cagr"], $reportDetails["revenue"], $reportDetails["key_highlight_image"]);

        unset($input["meta_description"], $input["meta_keywords"], $input["dc_description"], $input["cannonical_link"], $input["meta_title"], $input["toc"], $input["list_tbl_fig"]);
        $rdArray = $this->updateReportdetails($report, $reportDetails);
        // Uploading Image
        if (array_key_exists('featured_image', $input)) {
            $this->deleteOldFile($report->featured_image);
            $input = $this->uploadImage($input);
        }

        if (array_key_exists('key_highlight_image', $input)) {
            $this->deleteOldFile($report->key_highlight_image);
            $input = $this->uploadImage($input);
        }

        return DB::transaction(function () use ($report, $input, $tagsArray, $categoriesArray, $subcategoriesArray, $regionArray, $rdArray, $reportDetails) {

                    $input['slug'] = str_slug($reportDetails['meta_keywords']);
                    $input['updated_by'] = access()->user()->id;
                    $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);

                    if ($report->update($input)) {

                        // Inserting associated category's id in mapper table
                        if (count($categoriesArray)) {
                            $report->categories()->sync($categoriesArray);
                        }

                        // Inserting associated regionArray's id in mapper table
                        if (count($regionArray)) {
                            $report->Region()->sync($regionArray);
                        }


                        // Inserting associated tag's id in mapper table
                        if (count($tagsArray)) {
                            $report->tags()->sync($tagsArray);
                        }

                        // Inserting associated subcategory's id in mapper table
                        if (count($subcategoriesArray)) {
                            $report->subcategories()->sync($subcategoriesArray);
                        }

                        // Inserting associated subcategory's id in mapper table
                        if (count($rdArray)) {
                            $report->reportdetails()->sync($rdArray);
                        }
//                event(new ReportUpdated($report));

                        return true;
                    }

                    throw new GeneralException(
                    trans('exceptions.backend.reports.update_error')
                    );
                });
    }

    /**
     * Creating Tags.
     *
     * @param array $tags
     *
     * @return array
     */
    public function createTags($tags) {
        //Creating a new array for tags (newly created)
        $tags_array = [];

        foreach ($tags as $tag) {
            if (is_numeric($tag)) {
                $tags_array[] = $tag;
            } else {
                $newTag = ReportTag::create(['name' => $tag, 'status' => 1, 'created_by' => 1]);
                $tags_array[] = $newTag->id;
            }
        }

        return $tags_array;
    }

    /**
     * Creating Categories.
     *
     * @param Array($createsubCategories)
     *
     * @return array
     */
    public function createsubCategories($createsubCategories) {
        //Creating a new array for categories (newly created)
        $categories_array = [];

        foreach ($createsubCategories as $subcategory) {
            if (is_numeric($subcategory)) {
                $categories_array[] = $subcategory;
            } else {
                $newCategory = Reportsubcategory::create(['name' => $subcategory, 'status' => 1, 'created_by' => 1]);

                $categories_array[] = $newCategory->id;
            }
        }

        return $categories_array;
    }

    /**
     * Creating $region.
     *
     * @param Array($region)
     *
     * @return array
     */
    public function createRegion($regions) {
        //Creating a new array for categories (newly created)
        $region_array = [];

        foreach ($regions as $region) {
            if (is_numeric($region)) {
                $region_array[] = $region;
            } else {
                $newregion = Region::create(['name' => $region, 'status' => 1, 'created_by' => 1]);

                $region_array[] = $newregion->id;
            }
        }

        return $region_array;
    }

    /**
     * Creating Categories.
     *
     * @param Array($categories)
     *
     * @return array
     */
    public function createCategories($categories) {
        //Creating a new array for categories (newly created)
        $categories_array = [];

        foreach ($categories as $category) {
            if (is_numeric($category)) {
                $categories_array[] = $category;
            } else {
                $newCategory = ReportCategory::create(['name' => $category, 'status' => 1, 'created_by' => 1]);

                $categories_array[] = $newCategory->id;
            }
        }

        return $categories_array;
    }

    /**
     * @param \App\Models\Reports\Report $report
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Report $report) {
        $sold = DB::table('user_map_report')->where('report_id', $report->id)->first();
        //$customReport = CustomReport::where("report_id", $report->id)->first();
        if (isset($sold) && !empty($sold)) {
            return false;
        }
        if ($report->delete()) {
            ReportMapCategory::where('report_id', $report->id)->delete();
            ReportMapSubcategory::where('report_id', $report->id)->delete();
            ReportMapTags::where('report_id', $report->id)->delete();
            ReportMapRegion::where('report_id', $report->id)->delete();
            ReportMapRreportDetails::where('report_id', $report->id)->delete();
            $reportDetails = ReportDetails::find($report->id);
            if ($reportDetails) {
                $reportDetails->delete();
            }

            DB::table('client_profile_data')->where('report_id', $report->id)->delete();
            DB::table('custom_reportcharts')->where('report_id', $report->id)->delete();
            DB::table('custom_reports')->where('report_id', $report->id)->delete();
            DB::table('report_store')->where('report_id', $report->id)->delete();
            \DB::table("report_notifications")->where("primary_id", $report->id)->where("primary_table", "reports")->delete();


            return true;
        }

        throw new GeneralException(trans('exceptions.backend.reports.delete_error'));
    }

    /**
     * Creating $region.
     *
     * @param Array($region)
     *
     * @return array
     */
    public function createReportdetails($reportdetails) {
        //Creating a new array for categories (newly created)

        $rd_array = [];
        $reportdetails['created_by'] = access()->user()->id;
        if ($reportdetails = ReportDetails::create($reportdetails)) {
            $rd_array[] = $reportdetails->id;
        }
        return $rd_array;
    }

    /**
     * Creating $region.
     *
     * @param Array($region)
     *
     * @return array
     */
    public function updateReportdetails($report, $rarray) {
        $rdata = $report->reportDetails->toArray();
        $rdetailsId = $rdata[0]["id"];
        if ($rdetailsId) {
            $rd_array = [];
            $reportDetails = ReportDetails::find($rdetailsId);
            $rarray['updated_by'] = access()->user()->id;
            $reportdetails = $reportDetails->update($rarray);
            if ($reportdetails) {
                $rd_array[] = $rdetailsId;
            }
            return $rd_array;
        }
    }

    /**
     * Upload Image.
     *
     * @param array $input
     *
     * @return array $input
     */
    public function uploadImage($input) {
        if (isset($input['featured_image']) && !empty($input['featured_image'])) {
            $avatar = $input['featured_image'];
            $fileName = time() . $avatar->getClientOriginalName();

            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()), 'public');

            $input = array_merge($input, ['featured_image' => $fileName]);

            return $input;
        }

        if (isset($input['key_highlight_image']) && !empty($input['key_highlight_image'])) {
            $avatar = $input['key_highlight_image'];
            $fileName = time() . $avatar->getClientOriginalName();

            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()), 'public');

            $input = array_merge($input, ['key_highlight_image' => $fileName]);

            return $input;
        }
    }

    /**
     * Destroy Old Image.
     *
     * @param int $id
     */
    public function deleteOldFile($fileName = '') {
//        $fileName = $model->featured_image;

        return $this->storage->delete($this->upload_path . $fileName);
    }

}
