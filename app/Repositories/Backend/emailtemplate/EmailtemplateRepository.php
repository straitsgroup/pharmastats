<?php

namespace App\Repositories\Backend\emailtemplate;

use App\Models\emailtemplate\Emailtemplate;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;

/**
 * Class EmailtemplateRepository.
 */
class EmailtemplateRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Emailtemplate::class;

    public function __construct(Emailtemplate $emailtemplate) {
        $this->model = $emailtemplate;
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->select([
                            config('module.emailtemplates.table') . '.id',
                            config('module.emailtemplates.table') . '.template_code',
                            config('module.emailtemplates.table') . '.template_name',
                            config('module.emailtemplates.table') . '.template_html',
                            config('module.emailtemplates.table') . '.created_at',
                            config('module.emailtemplates.table') . '.updated_at',
        ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input) {
        if (Emailtemplate::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.emailtemplates.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Emailtemplate $emailtemplate
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Emailtemplate $emailtemplate, array $input) {
        if ($emailtemplate->update($input)){
            return true;            
        }
        throw new GeneralException(trans('exceptions.backend.emailtemplates.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Emailtemplate $emailtemplate
     * @throws GeneralException
     * @return bool
     */
    public function delete(Emailtemplate $emailtemplate) {
        if ($emailtemplate->delete()) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.emailtemplates.delete_error'));
    }
}
