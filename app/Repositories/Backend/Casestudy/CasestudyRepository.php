<?php

namespace App\Repositories\Backend\Casestudy;

use DB;
use App\Models\Casestudy\Casestudy;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

/**
 * Class CasestudyRepository.
 */
class CasestudyRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Casestudy::class;

    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct() {
        $this->upload_path = '/static/assets/img' . DIRECTORY_SEPARATOR . 'casestudy' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('do');
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->leftjoin(config('access.users_table'), config('access.users_table') . '.id', '=', config('module.casestudies.table') . '.created_by')
                        ->select([
                            config('module.casestudies.table') . '.id',
                            config('module.casestudies.table') . '.name',
                            config('module.casestudies.table') . '.publish_datetime',
                            config('module.casestudies.table') . '.status',
                            config('module.casestudies.table') . '.created_by',
                            config('module.casestudies.table') . '.created_at',
                            config('access.users_table') . '.first_name as user_name',
        ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input) {
        DB::transaction(function () use ($input) {
            $input['slug'] = str_slug($input['name']);
            $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
            $input = $this->uploadImage($input);
            $input['created_by'] = access()->user()->id;
//            dd($input);

            if ($casestudy = Casestudy::create($input)) {
//                event(new CasestudyCreated($casestudy));
                $notificationArr = [];
//                $notificationArr['report_id'] = $report->id;
                $notificationArr['report_cat_id'] = $input['domain'];
                $notificationArr['type'] = 1;
                $notificationArr['msg'] = "New Case Study published..! " . $input['name'];
                $notificationArr['status'] = 1;
                $notificationArr['primary_id'] = $casestudy->id;
                $notificationArr['primary_table'] = "casestudies";
                \DB::table("report_notifications")->insert($notificationArr);

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.casestudies.create_error'));
        });
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Casestudy $casestudy
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Casestudy $casestudy, array $input) {
        $input['slug'] = str_slug($input['name']);
        $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
        $input['updated_by'] = access()->user()->id;

        // Uploading Image
        if (array_key_exists('featured_image', $input)) {
            $this->deleteOldFile($casestudy);
            $input = $this->uploadImage($input);
        }

        DB::transaction(function () use ($casestudy, $input) {
            if ($casestudy->update($input)) {
//                event(new CasestudyUpdated($casestudy));
                return true;
            }

            throw new GeneralException(
            trans('exceptions.backend.casestudies.update_error')
            );
        });
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Casestudy $casestudy
     * @throws GeneralException
     * @return bool
     */
    public function delete(Casestudy $casestudy) {
        $id = $casestudy->id;
        if ($casestudy->delete()) {
            \DB::table("report_notifications")
                    ->where("primary_id", $id)
                    ->where("primary_table", "casestudies")
                    ->delete();
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.casestudies.delete_error'));
    }

    /**
     * Upload Image.
     *
     * @param array $input
     *
     * @return array $input
     */
    public function uploadImage($input) {
        $avatar = $input['featured_image'];

        if (isset($input['featured_image']) && !empty($input['featured_image'])) {
            $fileName = time() . $avatar->getClientOriginalName();

            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath(), 'public'));

            $input = array_merge($input, ['featured_image' => $fileName]);

            return $input;
        }
    }

    /**
     * Destroy Old Image.
     *
     * @param int $id
     */
    public function deleteOldFile($model) {
        $fileName = $model->featured_image;

        return $this->storage->delete($this->upload_path . $fileName);
    }

}
