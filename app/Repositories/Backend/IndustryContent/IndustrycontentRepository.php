<?php

namespace App\Repositories\Backend\IndustryContent;

use DB;
use Carbon\Carbon;
use App\Models\IndustryContent\Industrycontent;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class IndustrycontentRepository.
 */
class IndustrycontentRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Industrycontent::class;
    
    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct() {
        $this->upload_path = '/static/assets/img' . DIRECTORY_SEPARATOR . 'blog' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('do');
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable($tab_id) {
        return $this->query()
                        ->select([
                            config('module.industrycontents.table') . '.id',
                            config('module.industrycontents.table') . '.name',
                            config('module.industrycontents.table') . '.created_at',
                            config('module.industrycontents.table') . '.updated_at',
        ])->where("tab_id", $tab_id);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input) {
        if (isset($input['featured_image'])) {
            $input = $this->uploadImage($input);
        }
         $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
        if (Industrycontent::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.industrycontents.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Industrycontent $industrycontent
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update($industrycontent, array $input) {
//        dd($industrycontent);
        
        $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
        $input['updated_by'] = access()->user()->id;
        
        if (array_key_exists('featured_image', $input)) {
            $this->deleteOldFile($industrycontent);
            $input = $this->uploadImage($input);
        }
        
        if ($industrycontent->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.industrycontents.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Industrycontent $industrycontent
     * @throws GeneralException
     * @return bool
     */
    public function delete(Industrycontent $industrycontent) {
        if ($industrycontent->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.industrycontents.delete_error'));
    }

    public function uploadImage($input) {
        $avatar = $input['featured_image'];

        if (isset($input['featured_image']) && !empty($input['featured_image'])) {
            $fileName = time() . $avatar->getClientOriginalName();

            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()), 'public');

            $input = array_merge($input, ['featured_image' => $fileName]);

            return $input;
        }
    }
    
     public function deleteOldFile($model) {
        $fileName = $model->featured_image;

        return $this->storage->delete($this->upload_path . $fileName, 'public');
    }

}
