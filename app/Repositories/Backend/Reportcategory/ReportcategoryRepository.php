<?php

namespace App\Repositories\Backend\Reportcategorys;

use App\Events\Backend\Reportcategorys\ReportcategoryCreated;
use App\Events\Backend\Reportcategorys\ReportcategoryDeleted;
use App\Events\Backend\Reportcategorys\ReportcategoryUpdated;
use App\Exceptions\GeneralException;
use App\Models\Reportcategory\Reportcategory;
use App\Repositories\BaseRepository;
use DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class ReportcategorysRepository.
 */
class ReportcategorysRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Reportcategory::class;

    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct() {
        $this->upload_path = 'img' . DIRECTORY_SEPARATOR . 'reportcategory' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('do');
    }

    /**
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->leftjoin(config('access.users_table'), config('access.users_table') . '.id', '=', config('module.reportcategories.table') . '.created_by')
                        ->select([
                            config('module.reportcategories.table') . '.id',
                            config('module.reportcategories.table') . '.name',
                            config('module.reportcategories.table') . '.status',
                            config('module.reportcategories.table') . '.created_by',
                            config('module.reportcategories.table') . '.created_at',
                            config('access.users_table') . '.first_name as user_name',
        ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input) {

        if ($this->query()->where('name', $input['name'])->whereNull('deleted_at')->first()) {
            return false;
//            throw new GeneralException(trans('exceptions.backend.blogcategories.already_exists'));
        }
        return DB::transaction(function () use ($input) {
                    $slug = str_replace("&", "and", $input['name']);
                    $slug = str_replace(" ", "-", $slug);
                    $input['slug'] = $slug;

//                    $input['slug'] = str_slug($input['name']);
                    if (isset($input['featured_image']) && !empty($input['featured_image'])) {
                        $input = $this->uploadImage($input);
                    }
                    $input['created_by'] = access()->user()->id;

                    if (Reportcategory::create($input)) {
//                event(new ReportcategoryCreated($reportcategory));
                        return true;
                    }

//            throw new GeneralException(trans('exceptions.backend.reportcategorys.create_error'));
                });
    }

    /**
     * Update Reportcategory.
     *
     * @param \App\Models\Reportcategorys\Reportcategory $reportcategory
     * @param array                  $input
     */
    public function update(Reportcategory $reportcategory, array $input) {
//        dd($input);

        if ($this->query()->where('name', $input['name'])->where('id', '!=', $reportcategory->id)->first()) {
            return false;
//            throw new GeneralException(trans('exceptions.backend.reportcategorys.already_exists'));
        }
        $slug = str_replace("&", "and", $input['name']);
        $slug = str_replace(" ", "-", $slug);
        $input['slug'] = $slug;
//        $input['slug'] = str_slug($input['name']);
        $input['updated_by'] = access()->user()->id;

        // Uploading Image
        if (array_key_exists('featured_image', $input)) {
            $this->deleteOldFile($reportcategory);
            $input = $this->uploadImage($input);
        }

        return DB::transaction(function () use ($reportcategory, $input) {
                    if ($reportcategory->update($input)) {

//                event(new ReportcategoryUpdated($reportcategory));

                        return true;
                    }

                    throw new GeneralException(
                    trans('exceptions.backend.reportcategorys.update_error')
                    );
                });
    }

    /**
     * Creating Tags.
     *
     * @param array $tags
     *
     * @return array
     */
    public function createTags($tags) {
        //Creating a new array for tags (newly created)
        $tags_array = [];

        foreach ($tags as $tag) {
            if (is_numeric($tag)) {
                $tags_array[] = $tag;
            } else {
                $newTag = ReportcategoryTag::create(['name' => $tag, 'status' => 1, 'created_by' => 1]);
                $tags_array[] = $newTag->id;
            }
        }

        return $tags_array;
    }

    /**
     * Creating Categories.
     *
     * @param Array($categories)
     *
     * @return array
     */
    public function createCategories($categories) {
        //Creating a new array for categories (newly created)
        $categories_array = [];

        foreach ($categories as $category) {
            if (is_numeric($category)) {
                $categories_array[] = $category;
            } else {
                $newCategory = ReportcategoryCategory::create(['name' => $category, 'status' => 1, 'created_by' => 1]);

                $categories_array[] = $newCategory->id;
            }
        }

        return $categories_array;
    }

    /**
     * @param \App\Models\Reportcategorys\Reportcategory $reportcategory
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Reportcategory $reportcategory) {
        DB::transaction(function () use ($reportcategory) {
            if ($reportcategory->delete()) {

//                event(new ReportcategoryDeleted($reportcategory));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.reportcategorys.delete_error'));
        });
    }

    /**
     * Upload Image.
     *
     * @param array $input
     *
     * @return array $input
     */
    public function uploadImage($input) {
        $avatar = $input['featured_image'];

        if (isset($input['featured_image']) && !empty($input['featured_image'])) {
            $fileName = time() . $avatar->getClientOriginalName();

            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()), 'public');

            $input = array_merge($input, ['featured_image' => $fileName]);

            return $input;
        }
    }

    /**
     * Destroy Old Image.
     *
     * @param int $id
     */
    public function deleteOldFile($model) {
        $fileName = $model->featured_image;

        return $this->storage->delete($this->upload_path . $fileName);
    }

}
