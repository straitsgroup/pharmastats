<?php

namespace App\Repositories\Backend\UserMapIp;

use DB;
use Carbon\Carbon;
use App\Models\UserMapIp\UserMapIp;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserMapIpRepository.
 */
class UserMapIpRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = UserMapIp::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable($user_id) {
        return $this->query()
                        ->select([
                            config('module.usermapips.table') . '.id',
                            config('module.usermapips.table') . '.ip_address',
                            config('module.usermapips.table') . '.status',
                            config('module.usermapips.table') . '.remove_request',
                            config('module.usermapips.table') . '.created_at',
                            config('module.usermapips.table') . '.updated_at',
                        ])->where("user_id", $user_id);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input) {        
        if ($this->query()->where('ip_address', $input['ip_address'])->whereNull('deleted_at')->first()) {
            return false;
        }
        if (UserMapIp::create($input)) {
            return true;
        }
    }

    /**
     * For updating the respective Model in storage
     *
     * @param UserMapIp $usermapip
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(UserMapIp $usermapip, array $input) {
        if ($usermapip->remove_request == 1) {
            $input['remove_request'] = 2;
        }
        if ($usermapip->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.usermapips.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param UserMapIp $usermapip
     * @throws GeneralException
     * @return bool
     */
    public function delete(UserMapIp $usermapip) {
        if ($usermapip->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.usermapips.delete_error'));
    }

}
