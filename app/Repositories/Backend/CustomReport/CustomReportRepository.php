<?php

namespace App\Repositories\Backend\CustomReport;

use App\Exceptions\GeneralException;
use App\Models\CustomReport\CustomReport;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use App\Models\Region\Region;
use DB;
use App\Models\ReportStore\ReportStore;

/**
 * Class CustomReportRepository.
 */
class CustomReportRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = CustomReport::class;

    /**
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->select([
                            config('access.customreports_table') . '.id',
                            config('access.customreports_table') . '.report_id',
                            config('access.customreports_table') . '.name',
                            config('access.customreports_table') . '.created_by',
                            config('access.customreports_table') . '.created_at',
                            config('access.customreports_table') . '.updated_at',
                        ])->whereIn("form_type", [null, 1, 4])
                        ->where("region_id", "=", 1)
                        ->where("report_id", "!=", null)
                        ->orderBy("report_id");
    }

    public function getReportClientList($report_id, $region_id) {
        return $this->query()
                        ->select([
                            config('access.customreports_table') . '.id',
                            config('access.customreports_table') . '.report_id',
                            config('access.customreports_table') . '.name',
                            config('access.customreports_table') . '.client_profile_id',
                            config('access.customreports_table') . '.created_by',
                            config('access.customreports_table') . '.created_at',
                            config('access.customreports_table') . '.updated_at',
                        ])->where("form_type", "=", 2)
                        ->where("report_id", "=", $report_id)
                        ->where("region_id", "=", $region_id)
                        ->where("client_profile_id", "!=", 0);
    }

    public function getClientsForMappingList($report_id) {
        return $this->query()
                        ->select([
                            'clientprofiles.name as name',
                            'clientprofiles.id',
                        ])
                        ->rightJoin('clientprofiles', 'clientprofiles.id', '=', 'custom_reports.client_profile_id')
                        ->whereNotIn("clientprofiles.id", function($q) use ($report_id) {
                            $q->select("custom_reports.client_profile_id")
                            ->where("custom_reports.form_type", "=", 2)
                            ->where("custom_reports.report_id", "=", $report_id)
                            ->where("custom_reports.client_profile_id", "!=", 0);
                        });
    }

//    public function getRegionalCustomReports($report_id) {
//        return ReportStore::select([
//                            'report_store.id',
//                            'reports.name',
//                            'report_store.region_id',
//                            'report_store.created_by'
//                        ])
//                        ->join('reports', 'report_store.report_id', '=', 'reports.id')
//                        ->where("report_store.report_id", "=", $report_id)
//                        ->where("report_store.region_id", "!=", null);
//    }

    /**
     * 
     * @param type $report_id
     * @return type
     */
    public function getRegionalCustomReports($report_id) {
        return CustomReport::select([
                            'custom_reports.id',
                            'custom_reports.name',
                            'custom_reports.region_id',
                            'custom_reports.created_by',
                            'regions.name as region_name',
                        ])
                        ->join('regions', 'custom_reports.region_id', '=', 'regions.region_id')
                        ->where("custom_reports.report_id", "=", $report_id)
                        ->where("custom_reports.form_type", "=", 1)
                        ->where("custom_reports.region_id", "!=", 1);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     * create custom reports labels form
     */
    public function create(array $input) {
//        dd(json_decode($input['items']));
        
        if(isset($input['items']) && !empty($input['items'])){
            
            DB::table("form_map_fields")->where("form_id", $input['form_id'])->delete();
            $labelArr = json_decode($input['items']);
            $insertArr['form_id'] = $input['form_id'];
            
            foreach ($labelArr as $key => $value) {
                $insertArr['field_id'] = $value->label_id;
                
                DB::table("form_map_fields")->insert($insertArr);
            }
            
            return true;
            
        }
//        if ($this->query()->where('report_id', $input['report_id'])
//                        ->where('region_id', $input['region_id'])
//                        ->where('form_type', 1)
//                        ->first()) {
//            throw new GeneralException(trans('Custom Report already exists..!'));
//        } else {
//            if (isset($input['default_form']) && $input['default_form'] == 1) {
//                $items = CustomReport::select("items")->where("default_form", 1)->first();
//                $input['items'] = $items->items;
//            }

//            $name = DB::table('reports')->select('name')->where('id', $input['report_id'])->first();
//            $input['name'] = $name->name;
//            $input['created_by'] = access()->user()->id;
//            if (CustomReport::create($input)) {
//                return true;
//            }
//        }
        throw new GeneralException(trans('exceptions.backend.customreports.create_error'));
    }

    /**
     * @param \App\Models\CustomReport\CustomReport $customreport
     * @param  $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * return bool
     */
    public function update(CustomReport $customreport, array $input) {
//        if ($this->query()->where('name', $input['name'])->where('id', '!=', $customreport->id)->first()) {
//            throw new GeneralException(trans('exceptions.backend.customreports.already_exists'));
//        }

        $input['updated_by'] = access()->user()->id;

        if ($customreport->update($input)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.customreports.update_error'));
    }

    /**
     * @param \App\Models\CustomReport\CustomReport $customreport
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function delete(CustomReport $customreport) {
        if ($customreport->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.customreports.delete_error'));
    }

    /**
     * 
     * @param type $data
     * @param type $report_id
     * @return type
     * @throws \Exception
     */
    public function uploadExcelAndInsertChartData($data, $report_id) {
        try {


            $file = $data['Charts'];
//            $content = file_get_contents($file->getRealPath());
//            dd($content);

            $path = $file->getRealPath();

            $excel = Excel::load($path);

            $sheetNames = $excel->getSheetNames();



//            dd($sheetNames);

            $sheet = $excel->getSheet(0);

//            $sheetName = $excel->getSheetByName($sheet);
            $highestRow = $sheet->getHighestRow();

            $highestColumn = $sheet->getHighestColumn();

//            $maxCell = $sheet->getHighestRowAndColumn();
//            $rowdata = $sheet->rangeToArray('A1:' . $maxCell['column'] . $maxCell['row']);
//            
//            dd($rowdata);
            $emptyRowCount = 0;
            for ($row = 1; $row <= $highestRow; $row++) {
                //  Read a row of data into an array

                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

                if ($rowData[0][0] == null || $rowData[0][0] == '') {
//                    empty row detected
                    $emptyRowCount++;
                    if ($emptyRowCount == 2) {
                        break;
                    }
                    continue;
                }


//                dd($rowData);
//                $map[] = $rowData[0][0];
                if ($emptyRowCount == 1) {
                    $region = $rowData[0][0];
                    $revenue = [];
                } else {
                    $title = $rowData[0][0];
                }


//                dd($region);
                unset($rowData[0][0]);
                $years = [];
//                $revenue =[];
//                dd($rowData[0]);
                foreach ($rowData[0] as $key => $value) {

                    if ($value == null || $value == '') {
                        break;
                    }
//                    dd($emptyRowCount);
                    if ($emptyRowCount == 1) {
                        $years[] = $value;
                    } else {
                        $revenue[] = $value;
                    }
                }



                if ($emptyRowCount == 0) {
                    $map[$region][$title][] = $revenue;
                    $revenue = [];
                } else {
                    $map[$region]['years'] = $years;
                    $emptyRowCount = 0;
                    continue;
                }
            }

            dd($map);

            return $this->insertChartData($map, $report_id);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function insertChartData($chartDataArr, $report_id) {
//        dd($chartDataArr);

        $regions = \DB::table("regions")->pluck('name', 'region_id')->toArray();

//            dd($regions);

        foreach ($chartDataArr as $region => $data) {

            $insert["report_id"] = $report_id;
            $insert["region_id"] = array_search($region, $regions);
            $insert["data"] = json_encode($data);
            $insert["created_by"] = access()->user()->id;
//            $insert["created_at"] = ;

            $final[] = $insert;
        }

        return \DB::table("custom_reportcharts")->insert($final);
//        dd($final);
    }

    public function mapDataForEdit($charts) {
        foreach ($charts as $key => $chart) {
//            if($key == 0){
//                continue;
//            }
            $content = json_decode($chart->data);

            $years = $content->years;

            unset($content->years);

//            dd($content);

            foreach ($content as $k => $v) {
//                dd($v);
                for ($i = 0; $i < count($v); $i++) {
                    for ($j = 0; $j < count($v[$i]); $j++) {
                        $newArr[$years[$j]][] = $v[$i][$j];
                    }
                }
            }

            $editData[] = $newArr;
            $newArr = [];
        }
        dd($editData);
    }

    /**
     * 
     * @param type $data
     * @return type
     * upload multiple region charts in single file
     */
    public function storeChart($data) {

        $file = $data['Charts'];
        $path = $file->getRealPath();

        $excel = Excel::load($path);

        $sheetNames = $excel->getSheetNames();

        foreach ($sheetNames as $num => $sheetName) {
            $region = DB::table("regions")->select("region_id")->where("name", $sheetName)->first();
            $data['region'] = $region->region_id;

            $sheet = $excel->getSheet($num);
            $res = $this->storeChartSheet($data, $sheet);
        }

        return $res;
    }

    /*
     * upload excel sheet and store all tables in a single row
     * i.e. one region = one entry in database
     */

    public function storeChartSheet($data, $sheet) {
        $chartType = 0;
        if (isset($data['type']) && !empty($data['type'])) {
            $chartType = $data['type'];
        }

        try {
            $chartExists = \DB::table("custom_reportcharts")
                    ->where("report_id", $data['report_id'])
                    ->where("region_id", $data['region'])
                    ->where("type", $chartType)
                    ->where("chartdata_type", $data['chartdata_type'])
                    ->first();
            if (!empty($chartExists)) {
                \DB::table("custom_reportcharts")->where('id', $chartExists->id)->delete();
            }

            $configArr = config('app.regions');

            $regionArr = DB::table("regions")->pluck("name", "region_id")->toArray();

            $highestRow = $sheet->getHighestRow();

            $highestColumn = $sheet->getHighestColumn();
            $final = [];
            $descriptionArr = [];
            $city = '';

            for ($row = 1; $row <= $highestRow; $row++) {
                //  Read a row of data into an array

                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

                if ($rowData[0][0] == null || $rowData[0][0] == '') {
//                    empty row detected                 
                    break;
                } else if ($rowData[0][0] == "Years") {

                    unset($rowData[0][0]);
                    foreach ($rowData[0] as $key => $value) {
                        if ($value == null || $value == '') {
                            break;
                        }
                        $years[] = $value;
                    }

                    $insertArr['Years'] = json_encode($years);
                } else if ($rowData[0][0] == "CAGR") {
                    $insertArr["reg_cagr"] = $rowData[0][1];
                    continue;
                } else if ($rowData[0][0] == "Region" || $rowData[0][0] == "Country") {
                    $region = $configArr[array_search($rowData[0][1], $regionArr)];
                    $city = '';
                    continue;
                } else if ($rowData[0][0] == "City") {
                    $city = $configArr[array_search($rowData[0][1], $regionArr)];
                    continue;
                } else if ($rowData[0][0] == "Type") {
                    $type = $rowData[0][1];
                    continue;
                } else if ($rowData[0][0] == "Title") {
                    $charttitle = $rowData[0][1];
                    if (!empty($city) && isset($city)) {
                        $final[$region][$city][$type]['Title'] = $rowData[0][1];
                    } else {
                        $final[$region][$type]['Title'] = $rowData[0][1];
                    }
                    $insertArr['chart_title'] = $rowData[0][1];
                    continue;
                } else if ($rowData[0][0] == "Description") {
                    $chartdescription = $rowData[0][1];
                    if (!empty($city) && isset($city)) {
                        $descriptionArr[$configArr[$data['region']]][$region][$city][$type] = $rowData[0][1];
                    } else {
                        $descriptionArr[$configArr[$data['region']]][$region][$type] = $rowData[0][1];
                    }

                    continue;
                } else {
                    $label = $rowData[0][0];
                    unset($rowData[0][0]);
                    foreach ($rowData[0] as $key => $value) {
                        if ($value == null || $value == '') {
                            break;
                        }
                        if (!empty($city) && isset($city)) {
                            $final[$region][$city][$type][$label][] = $value;
                        } else {
                            $final[$region][$type][$label][] = $value;
                        }
                    }
                }
            }
            $arr[$configArr[$data['region']]] = $final;

            $insertArr['data'] = json_encode($arr);
//            dd($descriptionArr);
            if (isset($descriptionArr) && !empty($descriptionArr)) {
                $insertArr['chart_description'] = json_encode($descriptionArr);
            }
            $insertArr["report_id"] = $data['report_id'];
            $insertArr["region_id"] = $data['region'];
            if (!(isset($insertArr["reg_cagr"]))) {
                $insertArr["reg_cagr"] = $data['reg_cagr'];
            }
            if (!empty($data['country'])) {
                $insertArr["country_id"] = $data['country'];
            }
            if (!empty($data['city'])) {
                $insertArr["city_id"] = $data['city'];
            }
            $insertArr["created_by"] = access()->user()->id;
            $insertArr["type"] = $chartType;
            $insertArr["chartdata_type"] = $data['chartdata_type'];


            return \DB::table("custom_reportcharts")->insert($insertArr);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage() . ' Line - ' . $ex->getLine());
        }
    }

    /*
     * upload excel sheet and store each table as a separate row in table
     * i.e. one region = multiple entries in database
     */

    public function storeChart1($data) {
        try {
            $region = DB::table("regions")->where("region_id", $data['region'])->first();
            $file = $data['Charts'];
            $path = $file->getRealPath();

            $excel = Excel::load($path);

            $sheetNames = $excel->getSheetNames();
            $sheet = $excel->getSheet(0);
            $sheetTitle = $sheet->getTitle();
            if (strtolower($sheetTitle) != strtolower($region->name)) {
                return false;
            }
            $highestRow = $sheet->getHighestRow();

            $highestColumn = $sheet->getHighestColumn();

            $emptyRowCount = 0;
            for ($row = 1; $row <= $highestRow; $row++) {
                //  Read a row of data into an array

                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

                if ($rowData[0][0] == null || $rowData[0][0] == '') {
//                    empty row detected
                    $emptyRowCount++;
                    if ($emptyRowCount == 2) {
                        break;
                    }
                    continue;
                }

                if ($emptyRowCount == 1) {
                    $region = $rowData[0][0];
                    $revenue = [];
                } else {
                    $title = $rowData[0][0];
                }
                unset($rowData[0][0]);
                $years = [];
                foreach ($rowData[0] as $key => $value) {

                    if ($value == null || $value == '') {
                        break;
                    }
//                    dd($emptyRowCount);
                    if ($emptyRowCount == 1) {
                        $years[] = $value;
                    } else {
                        $revenue[] = $value;
                    }
                }
                if ($emptyRowCount == 0) {
                    $map[$region][$title] = $revenue;
                    $revenue = [];
                } else {
                    if ($row <= 2) {
                        $map[$region] = $years;
                    }

                    $emptyRowCount = 0;
                    continue;
                }
            }

            $insertYears = $map['Years'];

            unset($map['Years']);

            foreach ($map as $label => $val) {
                $insert["report_id"] = $data['report_id'];
                $insert["region_id"] = $data['region'];
                if (!empty($data['country'])) {
                    $insert["country_id"] = $data['country'];
                }
                $val["Years"] = $insertYears;
                $val["Title"] = $label;
                $insert["data"] = json_encode($val);
                $insert["created_by"] = access()->user()->id;

                $final[] = $insert;
            }

            return \DB::table("custom_reportcharts")->insert($final);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * 
     * @return type
     */
    public function getRegionalDataTable() {

        $report_ids = DB::table("custom_reports")
//                ->select('report_id')
                ->whereIn("form_type", [null, 1, 4])
                ->where("report_id", "!=", null)
                ->where("region_id", "=", 1)
                ->pluck("report_id")
                ->toArray();


        return $this->query()
                        ->select([
                            config('access.customreports_table') . '.id',
                            config('access.customreports_table') . '.report_id',
                            'custom_reports.region_id',
                            'custom_reports.name',
                            'custom_reports.created_by',
                            'custom_reports.created_at',
//                            config('access.customreports_table') . '.updated_at',
                            'regions.name as region_name',
                        ])
                        ->join('regions', 'custom_reports.region_id', '=', 'regions.region_id')
                        ->whereIn("form_type", [null, 1, 4])
                        ->where("custom_reports.region_id", "!=", 1)
                        ->where("report_id", "!=", null)
                        ->whereNotIn("report_id", $report_ids)
                        ->orderBy("report_id");
    }

}
