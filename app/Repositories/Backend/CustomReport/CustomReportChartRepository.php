<?php

namespace App\Repositories\Backend\CustomReport;

use App\Models\CustomReport\CustomReportChart;
use App\Repositories\BaseRepository;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class CustomReportRepository.
 */
class CustomReportChartRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = CustomReportChart::class;

    public function getForDataTable($id) {
        return $this->query()
                        ->select([
                            'custom_reportcharts.id',
                            'custom_reportcharts.region_id',
                            'custom_reportcharts.chartdata_type',
                            'regions.name', 'custom_reportcharts.created_at',
                            'custom_reportcharts.data'
                        ])->join("regions", function($query) {
                            $query->on("custom_reportcharts.region_id", "regions.region_id");
                        })
                        ->where("custom_reportcharts.report_id", $id)
                        ->where("custom_reportcharts.type", 0);
    }

    public function getSingleChartDataTable($type, $id, $region_id = 0) {
        return $this->query()
                        ->select([
                            'custom_reportcharts.id',
                            'custom_reportcharts.region_id',
                            'custom_reportcharts.region_id',
                            'custom_reportcharts.type',
                            'custom_reportcharts.chartdata_type',
                            'custom_reportcharts.chart_title',
                            'custom_reportcharts.data',
                            'custom_reportcharts.created_at'
                        ])
                        ->where("custom_reportcharts.report_id", $id)
                        ->where("custom_reportcharts.region_id", $region_id)
                        ->where("custom_reportcharts.type", $type);
    }

    public function getReportClientDataTable($report_id, $client_id) {
        return $this->query()
                        ->select([
                            'custom_reportcharts.id',
                            'custom_reportcharts.region_id',
                            'custom_reportcharts.region_id',
                            'custom_reportcharts.type',
                            'custom_reportcharts.chart_title',
                            'custom_reportcharts.data'
                        ])
                        ->where("custom_reportcharts.report_id", $report_id)
                        ->where("custom_reportcharts.client_profile_id", $client_id);
    }

    public function getEditChartData($id) {
        try {

            $response = \DB::table('custom_reportcharts')
                    ->select("custom_reportcharts.*", "regions.name")
                    ->join("regions", function($query) {
                        $query->on("custom_reportcharts.region_id", "regions.region_id");
                    })
                    ->where("id", $id)
                    ->first();

            return $response;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * 
     * @param type $data
     * @return type
     * @throws \Exception
     */
    public function updateChart($data) {
        try {
            $id = $data['id'];
            $title = $data['Title'];
            unset($data['Title']);
            unset($data['id']);
            if (isset($data['labelsNew'])) {
                foreach ($data['labelsNew'] as $index => $label) {
                    $new = explode("-", $label);
                    $data[$new[0]] = $new[1];
                }
            }
            unset($data['labelsNew']);
            $arr = $data;
            foreach ($data as $key => $value) {
                $arr[$key] = explode(",", $value);
            }
            $arr['Title'] = $title;

            return \DB::table("custom_reportcharts")->where("id", $id)
                            ->update(["data" => json_encode($arr)]);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * insert a newly created single chart
     * @param type $data
     * @return type
     * @throws \Exception
     */
    public function storeSingleChart($data) {
//        dd($data);
        try {
            $chartExists = \DB::table("custom_reportcharts")
                    ->where("report_id", $data['report_id'])
                    ->where("region_id", $data['region_id'])
                    ->where("type", $data['type'])
                    ->first();

            $defaultYear = date("Y", strtotime('-3 year'));
            $baseYear = !empty($data['base_year']) ? $data['base_year'] : (int) $defaultYear;
            $uptoYear = $data['upto_year'];
            for ($i = $baseYear; $i <= $uptoYear + 1; $i++) {
                $years[] = $i;
            }
//            type = 1 i.e. no geo heirarchy required
            if ($data['type'] == 1 || $data['type'] == 5) {
                $data['region'] = $data['region_id'];
                $data['country'] = 0;
                $insertData = $this->mapOtherTypeChartData($data);
//                dd($insertData);
                $insertData['Years'] = json_encode($years);
                return \DB::table("custom_reportcharts")->insert($insertData);
            }

//            type=2 i.e. client profile chart
            if ($data['type'] == 2) {
                $data['region'] = $data['region_id'];
                $data['country'] = 0;
                $insertData = $this->mapOtherTypeChartData($data);
                $insertData['Years'] = json_encode($years);
                return \DB::table("custom_reportcharts")->insert($insertData);
            }

            if (!empty($chartExists)) {
                $final = json_decode($chartExists->data, true);
                $updateData = $this->mapData($data, $final);
                return \DB::table("custom_reportcharts")->where("id", $chartExists->id)->update($updateData);
            } else {
                $insertData = $this->mapData($data);

                $insertData['Years'] = json_encode($years);
                return \DB::table("custom_reportcharts")->insert($insertData);
            }
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * 
     * @param type $data
     * @param type $final
     * @return type
     */
    public function mapData($data, $final = []) {
        $configArr = config('app.regions');
        $region = $configArr[$data['region']];
        $country = $region;
        $city = '';

        if (isset($data['country'])) {
            $insert['country_id'] = $data['country'];
            $country = $configArr[$data['country']];
        }

        if (isset($data['city'])) {
            $insert['city_id'] = $data['city'];
            $city = $configArr[$data['city']];
        }

        $chartData = json_decode($data['chart_data'], true);
        foreach ($chartData as $key => $value) {
            $value[] = 0; //pushing temp CAGR value at last position; need to calculate
            $legends[array_shift($value)] = $value;
        }
        $desc = [];

        if (isset($city) && !empty($city)) {
            $final[$region][$country][$city][$data['by_type']] = $legends;
            $final[$region][$country][$city][$data['by_type']]['Title'] = $data['title'];
            $final[$region][$country][$city][$data['by_type']] = $data['chart_description'];
        } else {
            $final[$region][$country][$data['by_type']] = $legends;
            $final[$region][$country][$data['by_type']]['Title'] = $data['title'];
            $desc[$region][$country][$data['by_type']] = $data['chart_description'];
        }

        $insert['data'] = json_encode($final);
        $insert['chart_description'] = json_encode($desc);
        $insert['region_id'] = $data['region'];
        $insert['report_id'] = $data['report_id'];
        $insert["created_by"] = access()->user()->id;
        $insert["type"] = $data['type'];
        $insert["chartdata_type"] = $data['chartdata_type'];
        return $insert;
    }

    /**
     * 
     * @param type $data
     * @param type $final
     * @return type
     * 
     */
    public function mapOtherTypeChartData($data, $final = []) {

        $chartData = json_decode($data['chart_data'], true);
        foreach ($chartData as $key => $value) {
            $value[] = 0; //pushing temp CAGR value at last position; need to calculate
            $legends[array_shift($value)] = $value;
        }
        $desc = [];

        if ($data['type'] == "1" || $data['type'] == "5") {
            $segment = "other";
        } else if ($data['type'] == "2") {
            $segment = "client";
        }

        $final[$data['type']][$data['report_id']][$segment] = $legends;
        $final[$data['type']][$data['report_id']][$segment]['Title'] = $data['title'];
        $desc[$data['type']][$data['report_id']][$segment] = $data['chart_description'];

        $insert['data'] = json_encode($final);
        $insert['chart_description'] = json_encode($desc);

        $insert['report_id'] = $data['report_id'];
        $insert["created_by"] = access()->user()->id;
        $insert["type"] = $data['type'];
        $insert["chart_title"] = $data['title'];
        $insert["chartdata_type"] = $data['chartdata_type'];
        $insert["region_id"] = $data['region_id'];
        return $insert;
    }

    /**
     * 
     * @param type $data
     * @param type $final
     * @return type
     * 
     */
    public function mapSingleChartData($data, $final = []) {
        $configArr = config('app.regions');
        $region = $configArr[$data['region']];
        $country = $configArr[$data['country']];
        $labels = array_values($data['labels']);
        foreach ($data['values'] as $k => $val) {
            $values[] = explode(",", $val);
        }
        $final[$region][$country][$data['by_type']] = array_combine($labels, $values);
        $final[$region][$country][$data['by_type']]['Title'] = $data['title'];

        $insert['data'] = json_encode($final);
        $insert['region_id'] = $data['region'];
        $insert['country_id'] = $data['country'];
        $insert['report_id'] = $data['report_id'];
        if (!empty($data['years'])) {
            $insert['years'] = $data['years'];
        }
        return $insert;
    }

    /**
     * updates chart data of single segment from region
     * @param type $data
     * @return type boolean
     * @throws \Exception
     */
    public function updateSegmentChart($data) {
        try {
            $params = $data['params'];
            $response = \DB::table("custom_reportcharts")->where("id", $data['params']['id'])->first();
            $originalData = json_decode($response->data, true);
            $newArr = $data['data'];
            if (isset($params['city']) && !empty($params['city'])) {
                $originalData[$params['region']][$params['country']][$params['segment']][$params['city']] = $newArr;
            } else {
                $originalData[$params['region']][$params['country']][$params['segment']] = $newArr;
            }
            return \DB::table("custom_reportcharts")->where("id", $data['params']['id'])
                            ->update(['data' => json_encode($originalData)]);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * 
     * @param type $data
     * upload other chart excel
     */
    public function uploadOtherCharts($data) {
        $file = $data['Charts'];
        $report_id = $data['report_id'];
        $path = $file->getRealPath();
        $excel = Excel::load($path);
        $sheet = $excel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $insertArr = [];
        $final = [];

        for ($row = 1; $row <= $highestRow; $row++) {
            //  Read a row of data into an array

            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            if ($rowData[0][0] == null || $rowData[0][0] == '') {
//                    empty row detected                 
                break;
            } else if ($rowData[0][0] == "Title") {
                if (!empty($final)) {
//                    insert here
                    $insertArr['data'] = json_encode($final);
                    $insertArr["report_id"] = $data['report_id'];
                    $insertArr["created_by"] = access()->user()->id;
                    $insertArr["type"] = 1;
                    $exist = CustomReportChart::where('chart_title', $insertArr['chart_title'])
                                    ->where('report_id', $insertArr['report_id'])->first();
                    if (isset($exist) && !empty($exist)) {
                        \DB::table("custom_reportcharts")->where('id', $exist->id)->update($insertArr);
                    } else {
                        \DB::table("custom_reportcharts")->insert($insertArr);
                    }
                    $final = [];
                }
                $charttitle = $rowData[0][1];
                $insertArr['chart_title'] = $charttitle;
                $final[1][$report_id]['other']['Title'] = $rowData[0][1];

                continue;
            } else if ($rowData[0][0] == "Segment") {
                $insertArr['segmental_type'] = $rowData[0][1];
                continue;
            } else if ($rowData[0][0] == "Years") {
                $years = [];

                unset($rowData[0][0]);
                foreach ($rowData[0] as $key => $value) {
                    if ($value == null || $value == '') {
                        break;
                    }
                    $years[] = $value;
                }

                $insertArr['years'] = json_encode($years);
                continue;
            } else {
                $label = $rowData[0][0];
                unset($rowData[0][0]);
                foreach ($rowData[0] as $key => $value) {
                    if ($value == null || $value == '') {
                        break;
                    }
                    $final[1][$report_id]['other'][$label][] = $value;
                }
            }
        }
        if (!empty($final)) {
            $insertArr['data'] = json_encode($final);
            $insertArr["report_id"] = $data['report_id'];
            $insertArr["created_by"] = access()->user()->id;
            $insertArr["type"] = 1;
            $exist = CustomReportChart::where('chart_title', $insertArr['chart_title'])
                            ->where('report_id', $insertArr['report_id'])->first();
            if (isset($exist) && !empty($exist)) {
                $response = \DB::table("custom_reportcharts")
                                ->where('id', $exist->id)->update($insertArr);
            } else {
                $response = \DB::table("custom_reportcharts")->insert($insertArr);
            }
            $final = [];
        }
        return $response;
    }

}
