<?php

namespace App\Repositories\Backend\LoginSessions;

use DB;
use Carbon\Carbon;
use App\Models\LoginSessions\LoginSession;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class LoginSessionRepository.
 */
class LoginSessionRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = LoginSession::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.loginsessions.table').'.id',
                config('module.loginsessions.table').'.user_id',
                config('module.loginsessions.table').'.ip_address',
                config('module.loginsessions.table').'.last_login',
                config('module.loginsessions.table').'.created_at',
                config('module.loginsessions.table').'.updated_at',
                'web_users.email',
            ])->join('web_users', 'web_users.id', '=', 'login_sessions.user_id');
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (LoginSession::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.loginsessions.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param LoginSession $loginsession
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(LoginSession $loginsession, array $input)
    {
    	if ($loginsession->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.loginsessions.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param LoginSession $loginsession
     * @throws GeneralException
     * @return bool
     */
    public function delete(LoginSession $loginsession)
    {
        if ($loginsession->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.loginsessions.delete_error'));
    }
}
