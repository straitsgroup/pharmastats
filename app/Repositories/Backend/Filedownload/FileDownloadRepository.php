<?php

namespace App\Repositories\Backend\Filedownload;

use DB;
use Carbon\Carbon;
use App\Models\Filedownload\FileDownload;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class FileDownloadRepository.
 */
class FileDownloadRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = FileDownload::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.filedownloads.table').'.id',
                config('module.filedownloads.table').'.template_name',
                config('module.filedownloads.table').'.html_code',
                config('module.filedownloads.table').'.created_at',
                config('module.filedownloads.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (FileDownload::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.filedownloads.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param FileDownload $filedownload
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(FileDownload $filedownload, array $input)
    {
    	if ($filedownload->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.filedownloads.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param FileDownload $filedownload
     * @throws GeneralException
     * @return bool
     */
    public function delete(FileDownload $filedownload)
    {
        if ($filedownload->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.filedownloads.delete_error'));
    }
}
