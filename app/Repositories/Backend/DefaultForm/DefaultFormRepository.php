<?php

namespace App\Repositories\Backend\DefaultForm;

use DB;
use Carbon\Carbon;
use App\Models\DefaultForm\DefaultForm;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DefaultFormRepository.
 */
class DefaultFormRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = DefaultForm::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.defaultforms.table').'.id',
                config('module.defaultforms.table').'.form_name',
                config('module.defaultforms.table').'.form_code',
                config('module.defaultforms.table').'.created_at',
                config('module.defaultforms.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (DefaultForm::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.defaultforms.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param DefaultForm $defaultform
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(DefaultForm $defaultform, array $input)
    {
    	if ($defaultform->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.defaultforms.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param DefaultForm $defaultform
     * @throws GeneralException
     * @return bool
     */
    public function delete(DefaultForm $defaultform)
    {
        if ($defaultform->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.defaultforms.delete_error'));
    }
}
