<?php

namespace App\Repositories\Backend\webuser;

use DB;
use Carbon\Carbon;
use App\Models\webuser\Webuser;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class WebuserRepository.
 */
class WebuserRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Webuser::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        //->join('packages', 'packages.id', '=', 'web_users.package_id')
                        ->select([
                            config('module.webusers.table') . '.id',
                            config('module.webusers.table') . '.first_name',
                            config('module.webusers.table') . '.last_name',
                            config('module.webusers.table') . '.email',
                            //config('module.webusers.table') . '.package_id',
                            config('module.webusers.table') . '.created_at',
                                // 'packages.name as package_name'
                        ])->where("user_type", 1);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create($input) {
//        $data = $request->except('password_confirmation');


        $input['status'] = isset($input['status']) ? $input['status'] : 1;
        $input['confirmation_code'] = md5(uniqid(mt_rand(), true));
        $input['confirmed'] = isset($input['confirmed']) ? $input['confirmed'] : 1;
        $input['is_active'] = isset($input['is_active']) ? $input['is_active'] : 1;
        $input['has_web_access'] = isset($input['has_web_access']) ? $input['has_web_access'] : 1;
        $input['created_by'] = access()->user()->id;

//        password encryption like django framework
        $password = $input['password'];

        $algorithm = "pbkdf2_sha256";
        $iterations = 150000;

        $newSalt = random_bytes(6);
        $newSalt = base64_encode($newSalt);

        $hash = hash_pbkdf2("SHA256", $password, $newSalt, $iterations, 0, true);
        $toDBStr = $algorithm . "$" . $iterations . "$" . $newSalt . "$" . base64_encode($hash);

        $input['password'] = $toDBStr;



        DB::transaction(function () use ($input) {
            if (Webuser::create($input)) {
                return true;
            }
            throw new GeneralException(trans('exceptions.backend.webusers.create_error'));
        });
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Webuser $webuser
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Webuser $webuser, array $input) {
        $input['updated_by'] = access()->user()->id;
        if ($webuser->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.webusers.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Webuser $webuser
     * @throws GeneralException
     * @return bool
     */
    public function delete(Webuser $webuser) {
        if ($webuser->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.webusers.delete_error'));
    }

}
