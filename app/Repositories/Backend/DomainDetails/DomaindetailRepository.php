<?php

namespace App\Repositories\Backend\DomainDetails;

use DB;
use Carbon\Carbon;
use App\Models\DomainDetails\Domaindetail;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DomaindetailRepository.
 */
class DomaindetailRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Domaindetail::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.domaindetails.table').'.id',
                config('module.domaindetails.table').'.created_at',
                config('module.domaindetails.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (Domaindetail::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.domaindetails.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Domaindetail $domaindetail
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Domaindetail $domaindetail, array $input)
    {
    	if ($domaindetail->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.domaindetails.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Domaindetail $domaindetail
     * @throws GeneralException
     * @return bool
     */
    public function delete(Domaindetail $domaindetail)
    {
        if ($domaindetail->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.domaindetails.delete_error'));
    }
}
