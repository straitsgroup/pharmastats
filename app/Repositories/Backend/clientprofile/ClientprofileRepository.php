<?php

namespace App\Repositories\Backend\clientprofile;

use DB;
use Carbon\Carbon;
use App\Models\clientprofile\Clientprofile;
use App\Models\clientprofile\ClientDetail;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Backend\CustomReport\CustomReportRepository;

/**
 * Class ClientprofileRepository.
 */
class ClientprofileRepository extends BaseRepository {

    protected $storage;
    protected $upload_path;

    /**
     * Associated Repository Model.
     */
    const MODEL = Clientprofile::class;

    public function __construct(CustomReportRepository $customReportRepository) {
        $this->upload_path = '/static/assets/img' . DIRECTORY_SEPARATOR . 'clientprofile' . DIRECTORY_SEPARATOR;
//        $this->upload_path = 'img' . DIRECTORY_SEPARATOR . 'clientprofile' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('do');
        $this->customReportRepository = $customReportRepository;
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->select([
                            config('module.clientprofiles.table') . '.id',
                            config('module.clientprofiles.table') . '.name',
                            config('module.clientprofiles.table') . '.status',
                            config('module.clientprofiles.table') . '.created_at',
                            config('module.clientprofiles.table') . '.updated_at',
        ]);
    }

    public function getClientsForMappingList($report_id, $region_id) {

        $db = $this->customReportRepository->getReportClientList($report_id, $region_id);
        return $this->query()
                        ->select([
                            'clientprofiles.name as name',
                            'clientprofiles.id',
                        ])
                        ->whereNotIn("id", $db->pluck("client_profile_id")->toArray());
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input) {
        $string = str_replace(' ', '-', $input['name']);
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $slug = str_replace('--', '-', $slug);
        $exists = Clientprofile::where("name", 'LIKE', $input['name'])->first();
        if (isset($exists) && !empty($exists)) {
            return false;
        }
        if (isset($input['client_logo'])) {
            $input = $this->uploadImage($input);
        }
//        $exists = Clientprofile::where("slug", $slug)->first();
//        if (isset($exists) && !empty($exists)) {
//            return false;
//        }
//        if (isset($input['client_logo'])) {
//            $input = $this->uploadImage($input);
//        }

        $input['slug'] = $slug;
        $input['created_by'] = access()->user()->id;

        if (Clientprofile::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.clientprofiles.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Clientprofile $clientprofile
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Clientprofile $clientprofile, array $input) {
        $input['updated_by'] = access()->user()->id;
        $exists = Clientprofile::where("name", 'LIKE', $input['name'])
                ->where('id', '!=', $clientprofile->id)
                ->first();

        if (isset($exists) && !empty($exists)) {
            return false;
        }
// Uploading Image
        if (array_key_exists('client_logo', $input)) {
            $this->deleteOldFile($clientprofile);
            $input = $this->uploadImage($input);
        }
        if ($clientprofile->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.clientprofiles.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Clientprofile $clientprofile
     * @throws GeneralException
     * @return bool
     */
    public function delete(Clientprofile $clientprofile) {
        $deleteCustomForm = \DB::table("custom_reports")->where("client_profile_id", $clientprofile->id)->delete();
        $deleteData = \DB::table("client_profile_data")->where("client_profile_id", $clientprofile->id)->delete();
        $deleteCharts = \DB::table("custom_reportcharts")->where("client_profile_id", $clientprofile->id)->delete();
        $this->deleteOldFile($clientprofile);

        if ($clientprofile->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.clientprofiles.delete_error'));
    }

    public function uploadImage($input) {
        $avatar = $input['client_logo'];
        if (isset($input['client_logo']) && !empty($input['client_logo'])) {
//        dd($avatar);
            $fileName = $avatar->getClientOriginalName();
//            $fileName = $avatar->originalName;
            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()), 'public');
            $input = array_merge($input, ['client_logo' => $fileName]);
            return $input;
        }
    }

    public function deleteOldFile($model) {
        $fileName = $model->client_logo;
        return $this->storage->delete($this->upload_path . $fileName);
    }

    function saveDetails($data) {
        if (isset($data['image']) && !empty($data['image'])) {
            $data = $this->uploadClientDetailImage($data);
        }
        if (isset($data['chart_id']) && !empty($data['chart_id'])) {
            $data['chart_id'] = implode(",", $data['chart_id']);
        }

        if (isset($data['client_details_id']) && !empty($data['client_details_id'])) {
            $data['updated_by'] = access()->user()->id;
            if (ClientDetail::find($data['client_details_id'])->update($data)) {
                return true;
            }
        } else {
            $data['created_by'] = access()->user()->id;
            if (ClientDetail::create($data)) {
                return true;
            }
        }
        throw new GeneralException(trans('exceptions.backend.clientprofiles.create_error'));
    }

    public function uploadClientDetailImage($input) {
        $avatar = $input['image'];
        $fileName = $avatar->getClientOriginalName();
        $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()), 'public');
        $input = array_merge($input, ['image' => $fileName]);
        return $input;
    }

}
