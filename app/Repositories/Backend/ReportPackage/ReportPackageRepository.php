<?php

namespace App\Repositories\Backend\ReportPackage;

use DB;
use Carbon\Carbon;
use App\Models\ReportPackage\ReportPackage;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ReportPackageRepository.
 */
class ReportPackageRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = ReportPackage::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.reportpackages.table').'.id',
                config('module.reportpackages.table').'.name',
                config('module.reportpackages.table').'.report_id',
                config('module.reportpackages.table').'.created_at',
                config('module.reportpackages.table').'.updated_at',
            ])->where("form_type", 1)->where("region_id", 1)->where("id","!=", 1);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (ReportPackage::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.reportpackages.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param ReportPackage $reportpackage
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(ReportPackage $reportpackage, array $input)
    {
    	if ($reportpackage->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.reportpackages.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param ReportPackage $reportpackage
     * @throws GeneralException
     * @return bool
     */
    public function delete(ReportPackage $reportpackage)
    {
        if ($reportpackage->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.reportpackages.delete_error'));
    }
}
