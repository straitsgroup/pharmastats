<?php

namespace App\Repositories\Backend\Clients;

use App\Events\Backend\Clients\ClientCreated;
use App\Events\Backend\Clients\ClientDeleted;
use App\Events\Backend\Clients\ClientUpdated;
use App\Exceptions\GeneralException;
use App\Models\Clients\Client;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class ClientsRepository.
 */
class ClientsRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Client::class;

    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct() {
        $this->upload_path = 'img' . DIRECTORY_SEPARATOR . 'client' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('do');
    }

    /**
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->leftjoin(config('access.users_table'), config('access.users_table') . '.id', '=', config('module.clients.table') . '.created_by')
                        ->select([
                            config('module.clients.table') . '.id',
                            config('module.clients.table') . '.name',
                            config('module.clients.table') . '.status',
                            config('module.clients.table') . '.created_by',
                            config('module.clients.table') . '.created_at',
                            config('access.users_table') . '.first_name as user_name',
        ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input) {

        DB::transaction(function () use ($input) {
            $input['slug'] = str_slug($input['name']);
            $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
            $input = $this->uploadImage($input);
            $input['created_by'] = access()->user()->id;

            if ($client = Client::create($input)) {
                event(new ClientCreated($client));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.clients.create_error'));
        });
    }

    /**
     * Update Client.
     *
     * @param \App\Models\Clients\Client $client
     * @param array                  $input
     */
    public function update(Client $client, array $input) {

        $input['slug'] = str_slug($input['name']);
        $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
        $input['updated_by'] = access()->user()->id;

        // Uploading Image
        if (array_key_exists('featured_image', $input)) {
            $this->deleteOldFile($client);
            $input = $this->uploadImage($input);
        }

        DB::transaction(function () use ($client, $input) {
            if ($client->update($input)) {
                event(new ClientUpdated($client));

                return true;
            }

            throw new GeneralException(
            trans('exceptions.backend.clients.update_error')
            );
        });
    }

    /**
     * Creating Tags.
     *
     * @param array $tags
     *
     * @return array
     */
    public function createTags($tags) {
        //Creating a new array for tags (newly created)
        $tags_array = [];

        foreach ($tags as $tag) {
            if (is_numeric($tag)) {
                $tags_array[] = $tag;
            } else {
                $newTag = ClientTag::create(['name' => $tag, 'status' => 1, 'created_by' => 1]);
                $tags_array[] = $newTag->id;
            }
        }

        return $tags_array;
    }

    /**
     * Creating Categories.
     *
     * @param Array($categories)
     *
     * @return array
     */
    public function createCategories($categories) {
        //Creating a new array for categories (newly created)
        $categories_array = [];

        foreach ($categories as $category) {
            if (is_numeric($category)) {
                $categories_array[] = $category;
            } else {
                $newCategory = ClientCategory::create(['name' => $category, 'status' => 1, 'created_by' => 1]);

                $categories_array[] = $newCategory->id;
            }
        }

        return $categories_array;
    }

    /**
     * @param \App\Models\Clients\Client $client
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Client $client) {
        DB::transaction(function () use ($client) {
            if ($client->delete()) {
                ClientMapCategory::where('client_id', $client->id)->delete();
                ClientMapTag::where('client_id', $client->id)->delete();

                event(new ClientDeleted($client));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.clients.delete_error'));
        });
    }

    /**
     * Upload Image.
     *
     * @param array $input
     *
     * @return array $input
     */
    public function uploadImage($input) {
        $avatar = $input['featured_image'];

        if (isset($input['featured_image']) && !empty($input['featured_image'])) {
            $fileName = time() . $avatar->getClientOriginalName();

            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()),'public');

            $input = array_merge($input, ['featured_image' => $fileName]);

            return $input;
        }
    }

    /**
     * Destroy Old Image.
     *
     * @param int $id
     */
    public function deleteOldFile($model) {
        $fileName = $model->featured_image;

        return $this->storage->delete($this->upload_path . $fileName);
    }

}
