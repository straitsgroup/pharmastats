<?php

namespace App\Repositories\Backend\ReportLabel;

use DB;
use Carbon\Carbon;
use App\Models\ReportLabel\Reportlabel;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ReportlabelRepository.
 */
class ReportlabelRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Reportlabel::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->select([
                            config('module.reportlabels.table') . '.id',
                            config('module.reportlabels.table') . '.created_at',
                            config('module.reportlabels.table') . '.updated_at',
        ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input) {
        if (empty($input['slug']) || !(isset($input['slug']))) {
            $input['slug'] = strtolower(str_replace(" ", "_", trim($input['name'])));
        }        
        if ($this->query()->where('name', $input['name'])->first()) {
            throw new GeneralException(trans('exceptions.backend.report.already_exists'));
        } else {
            $slug = str_replace(" ", "_", trim($input['name']));
            $input['created_by'] = access()->user()->id;
//            dd($input);
            if (Reportlabel::create($input)) {
                $label = Reportlabel::where('name', $input['name'])->first();
                $label_id = $label->id;
                return $label;
            }
        }

        throw new GeneralException(trans('exceptions.backend.reportlabels.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Reportlabel $reportlabel
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(array $input) {
        if (empty($input['slug']) || !(isset($input['slug']))) {
            $input['slug'] = strtolower(str_replace(" ", "_", trim($input['name'])));
        }        

        $input['updated_by'] = access()->user()->id;
        $label_id = $input['label_id'];
        unset($input['label_id']);
        unset($input['id']);

        $data = DB::table('form_fields')
                ->where('id', $label_id)
                ->update($input);
        return $data;

        throw new GeneralException(trans('exceptions.backend.reportlabels.update_error'));
    }

    public function delete(Reportlabel $reportlabel) {
        if ($reportlabel->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.reportlabel.delete_error'));
    }

}
