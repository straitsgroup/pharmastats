<?php

namespace App\Repositories\Backend\Services;

use DB;
use Carbon\Carbon;
use App\Models\Services\Service;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class ServiceRepository.
 */
class ServiceRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Service::class;
    
    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct() {
        $this->upload_path = '/static/assets/img' . DIRECTORY_SEPARATOR . 'blog' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('do');
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->select([
                            config('module.services.table') . '.id',
                            config('module.services.table') . '.title',
                            config('module.services.table') . '.created_at',
                            config('module.services.table') . '.updated_at',
        ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input) {
        $input['page_slug'] = str_slug($input['title']);
        
        $input = $this->uploadImage($input);
        $input['created_by'] = access()->user()->id;

        if (Service::create($input)) {
            return true;
        }
        return false;
//        throw new GeneralException(trans('exceptions.backend.services.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Service $service
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Service $service, array $input) {
        
        $input['page_slug'] = str_slug($input['title']);
        
        $input['updated_by'] = access()->user()->id;

        // Uploading Image
        if (array_key_exists('featured_image', $input)) {
            $this->deleteOldFile($blog);
            $input = $this->uploadImage($input);
        }
        
        if ($service->update($input))
            return true;
        
        return false;

//        throw new GeneralException(trans('exceptions.backend.services.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Service $service
     * @throws GeneralException
     * @return bool
     */
    public function delete(Service $service) {
        if ($service->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.services.delete_error'));
    }
    
    public function uploadImage($input) {
        $avatar = $input['featured_image'];

        if (isset($input['featured_image']) && !empty($input['featured_image'])) {
            $fileName = time() . $avatar->getClientOriginalName();

            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()), 'public');

            $input = array_merge($input, ['featured_image' => $fileName]);

            return $input;
        }
    }
    
    public function deleteOldFile($model) {
        $fileName = $model->featured_image;

        return $this->storage->delete($this->upload_path . $fileName, 'public');
    }

}
