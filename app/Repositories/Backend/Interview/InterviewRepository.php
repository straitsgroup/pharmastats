<?php

namespace App\Repositories\Backend\Interview;

use DB;
use Carbon\Carbon;
use App\Models\Interview\Interview;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class InterviewRepository.
 */
class InterviewRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Interview::class;

    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct() {
        $this->upload_path = '/static/assets/img' . DIRECTORY_SEPARATOR . 'blog' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('do');
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->select([
                            config('module.interviews.table') . '.id',
                            config('module.interviews.table') . '.title',
                            config('module.interviews.table') . '.created_at',
                            config('module.interviews.table') . '.updated_at',
        ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input) {
        if (isset($input['featured_image'])) {
            $input = $this->uploadImage($input);
        }
        if (isset($input['video_file'])) {
            $input = $this->uploadVideo($input);
        }

        if (Interview::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.interviews.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Interview $interview
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Interview $interview, array $input) {
        
        if (array_key_exists('featured_image', $input)) {
            $this->deleteOldFile($interview);
            $input = $this->uploadImage($input);
        }
        
        if (array_key_exists('video_file', $input)) {
            $this->deleteOldVideo($interview);
            $input = $this->uploadVideo($input);
        }
        
        if ($interview->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.interviews.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Interview $interview
     * @throws GeneralException
     * @return bool
     */
    public function delete(Interview $interview) {
        if ($interview->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.interviews.delete_error'));
    }

    public function uploadImage($input) {
        $avatar = $input['featured_image'];

        if (isset($input['featured_image']) && !empty($input['featured_image'])) {
            $fileName = time() . $avatar->getClientOriginalName();

            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()), 'public');

            $input = array_merge($input, ['featured_image' => $fileName]);

            return $input;
        }
    }

    public function uploadVideo($input) {
        $avatar = $input['video_file'];

        if (isset($input['video_file']) && !empty($input['video_file'])) {
            $fileName = time() . $avatar->getClientOriginalName();

            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()), 'public');

            $input = array_merge($input, ['video_file' => $fileName]);

            return $input;
        }
    }
    
    public function deleteOldFile($model) {
        $fileName = $model->featured_image;

        return $this->storage->delete($this->upload_path . $fileName, 'public');
    }
    
    public function deleteOldVideo($model) {
        $fileName = $model->video_file;

        return $this->storage->delete($this->upload_path . $fileName, 'public');
    }

}
