<?php

namespace App\Repositories\Backend\UserMapPackage;

use DB;
use Carbon\Carbon;
use App\Models\UserMapPackage\UserMapPackage;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserMapPackageRepository.
 */
class UserMapPackageRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = UserMapPackage::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->select([
                            config('module.usermappackages.table') . '.id',
                            config('module.usermappackages.table') . '.created_at',
                            config('module.usermappackages.table') . '.updated_at',
        ]);
    }

    public function getReportUserPackage($report_id) {
        return $this->query()
                        ->select([
                            config('module.usermappackages.table') . '.id',
                            config('module.usermappackages.table') . '.user_id',
                            config('module.usermappackages.table') . '.package_id',
                            config('module.usermappackages.table') . '.status',
                            config('module.usermappackages.table') . '.subscription_expiry_date',
                            config('module.usermappackages.table') . '.created_at',
                            config('module.usermappackages.table') . '.updated_at',
                            'web_users.first_name',
                            'web_users.last_name',
                            'web_users.email',
                            'packages.price',
                            'packages.permissions',
                        ])->join("web_users", function($query) {
                            $query->on("web_users.id", "user_map_package.user_id");
                        })
                        ->join("packages", function($query) {
                            $query->on("packages.id", "user_map_package.package_id");
                        })
                        ->where("report_id", $report_id);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input) {
        if (UserMapPackage::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.usermappackages.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param UserMapPackage $usermappackage
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(UserMapPackage $usermappackage, array $input) {
        if ($usermappackage->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.usermappackages.update_error'));
    }

}
