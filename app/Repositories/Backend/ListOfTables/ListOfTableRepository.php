<?php

namespace App\Repositories\Backend\ListOfTables;

use DB;
use Carbon\Carbon;
use App\Models\ListOfTables\ListOfTable;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class ListOfTableRepository.
 */
class ListOfTableRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = ListOfTable::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->select([
                            config('module.listoftables.table') . '.id',
                            config('module.listoftables.table') . '.created_at',
                            config('module.listoftables.table') . '.updated_at',
        ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input) {
        if (ListOfTable::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.listoftables.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param ListOfTable $listoftable
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(ListOfTable $listoftable, array $input) {
        if ($listoftable->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.listoftables.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param ListOfTable $listoftable
     * @throws GeneralException
     * @return bool
     */
    public function delete(ListOfTable $listoftable) {
        if ($listoftable->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.listoftables.delete_error'));
    }

    /**
     * 
     * @param type $data
     * @return type
     * upload tables excel form for rd
     */
    public function updateTable($data) {
        $response = \DB::table("list_of_tables")
                        ->where("id", $data['id'])->update(["data" => $data['data']]);
        return $response;
    }

    /**
     * 
     * @param type $data
     * @return boolean
     * upload table excel for report labels
     */
    public function uploadTables($data) {
        $file = $data['tables'];

        $insertArr['report_id'] = $data['report_id'];
        $insertArr['region_id'] = $data['region_id'];
        $insertArr['client_profile_id'] = $data['client_profile_id'];
        $path = $file->getRealPath();

        $excel = Excel::load($path);
        $sheet = $excel->getSheet(0);

        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $heads = [];
        $tableArr = [];

        for ($row = 1; $row <= $highestRow + 1; $row++) {
            //  Read a row of data into an array

            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            if ($rowData[0][0] == null || $rowData[0][0] == '') {

                if (!empty($heads) && !empty($tableArr)) {

                    foreach ($tableArr as $index => $row1) {
                        $dataJson[] = json_encode(array_combine($heads, $row1));
                    }
                    $jsonStr = '[' . implode(",", $dataJson) . ']';

                    $insertArr['data'] = $jsonStr;


                    DB::table("list_of_tables")->insert($insertArr);
                }
                break;
            } else if ($rowData[0][0] == "slug") {

                if (!empty($heads) && !empty($tableArr)) {

                    foreach ($tableArr as $index => $row2) {
                        $dataJson[] = json_encode(array_combine($heads, $row2));
                    }
                    $jsonStr = '[' . implode(",", $dataJson) . ']';

                    $insertArr['data'] = $jsonStr;

                    $exist = DB::table("list_of_tables")
                            ->where("report_id", $data['report_id'])
                            ->where("region_id", $data['region_id'])
                            ->where("client_profile_id", $data['client_profile_id'])
                            ->get();

                    if (isset($exist) && !empty($excel)) {
                        DB::table("list_of_tables")
                                ->where("report_id", $data['report_id'])
                                ->where("region_id", $data['region_id'])
                                ->where("client_profile_id", $data['client_profile_id'])
                                ->delete();
                    }

                    $res = DB::table("list_of_tables")->insert($insertArr);

                    $insertArr = [];
                    $dataJson = [];
                    $jsonStr = '';
                    $insertArr['slug'] = $rowData[0][1];
                    $insertArr['report_id'] = $data['report_id'];
                    $insertArr['region_id'] = $data['region_id'];
                    $insertArr['client_profile_id'] = $data['client_profile_id'];
                    $dataArr = [];
                    $heads = [];
                    $tableArr = [];

                    continue;
                } else {

                    $insertArr['slug'] = $rowData[0][1];
                    $dataArr = [];
                    $heads = [];
                    $tableArr = [];
                }
                continue;
            } else if ($rowData[0][0] == "title") {
                $insertArr['title'] = $rowData[0][1];
                continue;
            } else {
                if (!empty($heads)) {

                    $rowArr = [];
                    foreach ($rowData[0] as $key1 => $value1) {
                        if ($value1 == null || $value1 == '') {
                            break;
                        }
                        $rowArr[] = $value1;
                    }
                    $tableArr[] = $rowArr;
                } else {

                    foreach ($rowData[0] as $key => $value) {
                        if ($value == null || $value == '') {
                            break;
                        }
                        $heads[] = $value;
                    }
                }
            }
        }
        return true;
    }

}
