<?php

namespace App\Repositories\Backend\Subscriptionchapters;

use DB;
use Carbon\Carbon;
use App\Models\Subscriptionchapters\SubscriptionChapter;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SubscriptionChapterRepository.
 */
class SubscriptionChapterRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = SubscriptionChapter::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->select([
                            config('module.subscriptionchapters.table') . '.id',
                            config('module.subscriptionchapters.table') . '.report_id',
                            config('module.subscriptionchapters.table') . '.name',
                            config('module.subscriptionchapters.table') . '.created_at',
                            config('module.subscriptionchapters.table') . '.updated_at',
        ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input) {
        if (SubscriptionChapter::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.subscriptionchapters.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param SubscriptionChapter $subscriptionchapter
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(SubscriptionChapter $subscriptionchapter, array $input) {
        if ($subscriptionchapter->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.subscriptionchapters.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param SubscriptionChapter $subscriptionchapter
     * @throws GeneralException
     * @return bool
     */
    public function delete(SubscriptionChapter $subscriptionchapter) {
        if ($subscriptionchapter->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.subscriptionchapters.delete_error'));
    }

    public function getReportPackage($report_id) {
        return $this->query()
                        ->select([
                            config('module.subscriptionchapters.table') . '.id',
                            config('module.subscriptionchapters.table') . '.report_id',
                            config('module.subscriptionchapters.table') . '.name',
                            config('module.subscriptionchapters.table') . '.no_of_pages',
                            config('module.subscriptionchapters.table') . '.single_price',
                            config('module.subscriptionchapters.table') . '.multiple_price',
                            config('module.subscriptionchapters.table') . '.enterprise_price',
                            config('module.subscriptionchapters.table') . '.type',
                            config('module.subscriptionchapters.table') . '.status',
                            config('module.subscriptionchapters.table') . '.created_at',
                            config('module.subscriptionchapters.table') . '.updated_at',
        ])->where("report_id", $report_id);
    }

}
