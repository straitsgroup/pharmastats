<?php

namespace App\Repositories\Backend\ReportChart;

use App\Models\ReportChart\Reportchart;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Events\Backend\Reportchart\ReportchartCreated;
use App\Events\Backend\Reportchart\ReportchartDeleted;
use App\Events\Backend\Reportchart\ReportchartUpdated;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class ReportchartRepository.
 */
class ReportchartRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Reportchart::class;

    protected $upload_path;
    protected $storage;

    public function __construct() {
        $this->upload_path = 'img' . DIRECTORY_SEPARATOR . 'reportcharts' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('do');
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()->leftjoin(config('access.users_table'), config('access.users_table') . '.id', '=', config('module.reportcharts.table') . '.created_by')
                        ->where(config('module.reportcharts.table') . '.report_id', '=', $_POST["report_id"])
                        ->select([
                            config('module.reportcharts.table') . '.id',
                            config('module.reportcharts.table') . '.title',
                            config('module.reportcharts.table') . '.status',
                            config('module.reportcharts.table') . '.created_at',
                            config('module.reportcharts.table') . '.created_by',
                            config('access.users_table') . '.first_name as user_name',
        ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input) {
        DB::transaction(function () use ($input) {
            $input = $this->uploadImage($input);
            $input['created_by'] = access()->user()->id;
            if ($reportchart = Reportchart::create($input)) {

                event(new ReportchartCreated($reportchart));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.reportcharts.create_error'));
        });
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Reportchart $reportchart
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Reportchart $reportchart, array $input) {
        if ($reportchart->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.reportcharts.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Reportchart $reportchart
     * @throws GeneralException
     * @return bool
     */
    public function delete(Reportchart $reportchart) {
        if ($reportchart->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.reportcharts.delete_error'));
    }

    public function uploadImage($input) {
        $avatar = $input['featured_image'];
        if (isset($input['featured_image']) && !empty($input['featured_image'])) {
            $fileName = time() . $avatar->getClientOriginalName();
            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()),'public');
            $input = array_merge($input, ['featured_image' => $fileName]);
            return $input;
        }
    }

    public function deleteOldFile($model) {
        $fileName = $model->featured_image;

        return $this->storage->delete($this->upload_path . $fileName);
    }

}
