<?php

namespace App\Repositories\Backend\CustomForms;

use DB;
use Carbon\Carbon;
use App\Models\CustomForms\CustomForm;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Models\CustomReport\CustomReport;

/**
 * Class CustomFormRepository.
 */
class CustomFormRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = CustomForm::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
                ->join("clientprofiles", function($query) {
                            $query->on("clientprofiles.id", "custom_reports.report_id");
                        })
            ->select([
                'clientprofiles.name as client_name',
                config('module.customforms.table').'.id',
                config('module.customforms.table').'.name',
                config('module.customforms.table').'.report_id',
                config('module.customforms.table').'.form_type',
                config('module.customforms.table').'.created_at',
                config('module.customforms.table').'.updated_at',
            ])->where(config('module.customforms.table').'.form_type', 2);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        $clientName = \DB::table("clientprofiles")->where("id", $input['client_profile_id'])->first();
        $input['name'] = $clientName->name;
        if (CustomForm::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.customforms.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param CustomForm $customform
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(CustomForm $customform, array $input)
    {
        if(isset($input['default_form']) && $input['default_form'] == 2){
                $items = CustomReport::select("items")->where("default_form", 2)->first();
                $input['items'] = $items->items;
            }
    	if ($customform->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.customforms.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param CustomForm $customform
     * @throws GeneralException
     * @return bool
     */
    public function delete(CustomForm $customform)
    {
        if ($customform->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.customforms.delete_error'));
    }
}
