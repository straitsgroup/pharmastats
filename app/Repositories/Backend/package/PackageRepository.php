<?php

namespace App\Repositories\Backend\package;

use DB;
use Carbon\Carbon;
use App\Models\package\Package;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class PackageRepository.
 */
class PackageRepository extends BaseRepository {

    protected $storage;
    protected $upload_path;

    /**
     * Associated Repository Model.
     */
    const MODEL = Package::class;

    public function __construct() {
        $this->upload_path = 'img' . DIRECTORY_SEPARATOR . 'packages' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('do');
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->select([
                            config('module.packages.table') . '.id',
                            config('module.packages.table') . '.name',
                            config('module.packages.table') . '.price',
                            config('module.packages.table') . '.created_at',
                            config('module.packages.table') . '.updated_at',
        ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input) {
        if (isset($input['image'])) {
            $input = $this->uploadImage($input);
        }
        $input['created_by'] = access()->user()->id;
        if (Package::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.packages.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Package $package
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Package $package, array $input) {
        $input['updated_by'] = access()->user()->id;
// Uploading Image
        if (array_key_exists('image', $input)) {
            $this->deleteOldFile($package);
            $input = $this->uploadImage($input);
        }

        if ($package->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.packages.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Package $package
     * @throws GeneralException
     * @return bool
     */
    public function delete(Package $package) {
        if ($package->id != 1) {
            if ($package->delete()) {
                return true;
            }
        } else {
            throw new GeneralException("Default Package can't be Delete......");
        }
    }

    /**
     * For Upload image in public/storage/packages
     *
     * @param insert inputs
     * @throws GeneralException
     * @return bool
     */
    public function uploadImage($input) {
        $avatar = $input['image'];
        if (isset($input['image']) && !empty($input['image'])) {
            $fileName = $avatar->getClientOriginalName();
            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()),'public');
            $input = array_merge($input, ['image' => $fileName]);
            return $input;
        }
    }

    public function deleteOldFile($model) {
        $fileName = $model->image;
        return $this->storage->delete($this->upload_path . $fileName);
    }
    
    public function getAll($order_by = 'id', $sort = 'asc')
    {
        return $this->query()            
            ->orderBy($order_by, $sort)
                ->pluck('name', 'id')->toArray();
//            ->get();
    }

}
