<?php

namespace App\Repositories\Backend\OtherCharts;

use DB;
use Carbon\Carbon;
use App\Models\OtherCharts\OtherChart;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OtherChartRepository.
 */
class OtherChartRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = OtherChart::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.othercharts.table').'.id',
                config('module.othercharts.table').'.type',
                config('module.othercharts.table').'.created_at',
                config('module.othercharts.table').'.updated_at',
            ])->where("type", "!=", "0");
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (OtherChart::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.othercharts.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param OtherChart $otherchart
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(OtherChart $otherchart, array $input)
    {
    	if ($otherchart->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.othercharts.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param OtherChart $otherchart
     * @throws GeneralException
     * @return bool
     */
    public function delete(OtherChart $otherchart)
    {
        if ($otherchart->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.othercharts.delete_error'));
    }
}
