<?php

namespace App\Repositories\Backend\Reportsubcategorys;

use App\Events\Backend\Reportsubcategorys\ReportsubcategoryCreated;
use App\Events\Backend\Reportsubcategorys\ReportsubcategoryDeleted;
use App\Events\Backend\Reportsubcategorys\ReportsubcategoryUpdated;
use App\Exceptions\GeneralException;
use App\Models\Reportcategory\Reportcategory;
use App\Models\SubcategoryMapCategories\SubcategoryMapCategory;
use App\Models\Reportsubcategory\Reportsubcategory;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class ReportsubcategorysRepository.
 */
class ReportsubcategorysRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Reportsubcategory::class;

    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct() {
        $this->upload_path = 'img' . DIRECTORY_SEPARATOR . 'reportsubcategory' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('do');
    }

    /**
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->leftjoin(config('access.users_table'), config('access.users_table') . '.id', '=', config('module.reportsubcategories.table') . '.created_by')
                        ->select([
                            config('module.reportsubcategories.table') . '.id',
                            config('module.reportsubcategories.table') . '.name',
                            config('module.reportsubcategories.table') . '.status',
                            config('module.reportsubcategories.table') . '.created_by',
                            config('module.reportsubcategories.table') . '.created_at',
                            config('access.users_table') . '.first_name as user_name',
        ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input) {
        if ($this->query()->where('name', $input['name'])->whereNull('deleted_at')->first()) {
            return false;
//            throw new GeneralException(trans('exceptions.backend.reportsubcategories.already_exists'));
        }
        $categoriesArray = $this->createCategories($input['categories']);
        unset($input['categories']);
        return DB::transaction(function () use ($input, $categoriesArray) {
            $input['slug'] = str_slug($input['name']);
            if (array_key_exists('featured_image', $input)) {
               $input = $this->uploadImage($input);
            }
            $input['created_by'] = access()->user()->id;

            if ($reportsubcategory = Reportsubcategory::create($input)) {
                // Inserting associated category's id in mapper table
                if (count($categoriesArray)) {
                    $reportsubcategory->categories()->sync($categoriesArray);
                }

//                event(new ReportsubcategoryCreated($reportsubcategory));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.reportsubcategories.create_error'));
        });
    }

    /**
     * Update Reportsubcategory.
     *
     * @param \App\Models\Reportsubcategorys\Reportsubcategory $reportsubcategory
     * @param array                  $input
     */
    public function update(Reportsubcategory $reportsubcategory, array $input) {

        if ($this->query()->where('name', $input['name'])->where('id', '!=', $reportsubcategory->id)->first()) {
            return false;
//            throw new GeneralException(trans('exceptions.backend.reportsubcategories.already_exists'));
        }

        $categoriesArray = $this->createCategories($input['categories']);
        unset($input['categories']);

        $input['slug'] = str_slug($input['name']);
        $input['updated_by'] = access()->user()->id;

        // Uploading Image
        if (array_key_exists('featured_image', $input)) {
            $this->deleteOldFile($reportsubcategory);
            $input = $this->uploadImage($input);
        }

        return DB::transaction(function () use ($reportsubcategory, $input, $categoriesArray) {
            if ($reportsubcategory->update($input)) {

                // Updateing associated category's id in mapper table
                if (count($categoriesArray)) {
                    $reportsubcategory->categories()->sync($categoriesArray);
                }

//                event(new ReportsubcategoryUpdated($reportsubcategory));

                return true;
            }

            throw new GeneralException(
            trans('exceptions.backend.reportsubcategories.update_error')
            );
        });
    }

    /**
     * Creating Tags.
     *
     * @param array $tags
     *
     * @return array
     */
    public function createTags($tags) {
        //Creating a new array for tags (newly created)
        $tags_array = [];

        foreach ($tags as $tag) {
            if (is_numeric($tag)) {
                $tags_array[] = $tag;
            } else {
                $newTag = ReportsubcategoryTag::create(['name' => $tag, 'status' => 1, 'created_by' => 1]);
                $tags_array[] = $newTag->id;
            }
        }

        return $tags_array;
    }

    /**
     * Creating Categories.
     *
     * @param Array($categories)
     *
     * @return array
     */
    public function createCategories($categories) {
        //Creating a new array for categories (newly created)
        $categories_array = [];

        foreach ($categories as $category) {
            if (is_numeric($category)) {
                $categories_array[] = $category;
            } else {
                $newCategory = Reportcategory::create(['name' => $category, 'status' => 1, 'created_by' => 1]);

                $categories_array[] = $newCategory->id;
            }
        }

        return $categories_array;
    }

    /**
     * @param \App\Models\Reportsubcategorys\Reportsubcategory $reportsubcategory
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Reportsubcategory $reportsubcategory) {
        DB::transaction(function () use ($reportsubcategory) {
            if ($reportsubcategory->delete()) {
                SubcategoryMapCategory::where('reportsubcat_id', $reportsubcategory->id)->delete();

//                event(new ReportsubcategoryDeleted($reportsubcategory));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.reportsubcategories.delete_error'));
        });
    }

    /**
     * Upload Image.
     *
     * @param array $input
     *
     * @return array $input
     */
    public function uploadImage($input) {
        $avatar = $input['featured_image'];

        if (isset($input['featured_image']) && !empty($input['featured_image'])) {
            $fileName = time() . $avatar->getClientOriginalName();

            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()),'public');

            $input = array_merge($input, ['featured_image' => $fileName]);

            return $input;
        }
    }

    /**
     * Destroy Old Image.
     *
     * @param int $id
     */
    public function deleteOldFile($model) {
        $fileName = $model->featured_image;

        return $this->storage->delete($this->upload_path . $fileName);
    }

}
