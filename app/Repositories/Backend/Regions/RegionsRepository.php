<?php

namespace App\Repositories\Backend\Regions;

use App\Events\Backend\Regions\RegionCreated;
use App\Events\Backend\Regions\RegionDeleted;
use App\Events\Backend\Regions\RegionUpdated;
use App\Exceptions\GeneralException;
use App\Models\Region\Region;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

/**
 * Class RegionsRepository.
 */
class RegionsRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Region::class;

    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct() {
        $this->upload_path = 'img' . DIRECTORY_SEPARATOR . 'region' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('do');
    }

    /**
     * @return mixed
     */
    public function getForDataTable($region_id = 1) {
        return $this->query()
//                        ->leftjoin(config('access.users_table'), config('access.users_table') . '.id', '=', config('module.regions.table') . '.created_by')
//                        ->where('regions.type', 1)
                        ->where('regions.parent', $region_id)
                        ->select([
                            config('module.regions.table') . '.region_id',
                            config('module.regions.table') . '.name',
                            config('module.regions.table') . '.type',
                            config('module.regions.table') . '.status',
                            config('module.regions.table') . '.created_by',
                            config('module.regions.table') . '.created_at',
//                            config('access.users_table') . '.first_name as user_name',
        ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input, $request) {
        if (isset($input['name']) && !empty($input)) {
            if ($this->query()->where('name', $input['name'])->first()) {
                return false;
            }
            $insertArr['name'] = $input['name'];
            $insertArr['type'] = 3;
            $insertArr['parent'] = $input['country'];
            $insertArr['super_parent'] = $input['region'];
            $insertArr['created_by'] = access()->user()->id;

            $conf = config('app');
            $conf["regions"][] = $input['name'];
            $conf["countries"][$input['name']] = $input['name'];
//            $data = var_export($conf, 1);
            if (Region::create($insertArr)) {
//                if (\File::put(config_path() . '/app.php', "<?php\n return $data ;")) {
                    return true;                    
//                }
            }
        } else {
            $path = $request->file('cities')->getRealPath();
            $data = \Excel::load($path)->get();
            if ($data->count() > 0) {
                $conf = config('app');
                foreach ($data->toArray() as $key => $value) {
                    foreach ($value as $row) {
                        if ($row != null) {
                            if ($this->query()->where('name', $row)->first()) {
                                continue;
                            }
                            $conf["regions"][] = $row;
                            $conf["countries"][$row] = $row;
                            $insert_data[] = array(
                                'name' => $row,
                                'type' => 3,
                                'parent' => $input['country'],
                                'super_parent' => $input['region'],
                                'created_by' => access()->user()->id,
                                'created_at' => \Carbon\Carbon::now(),
                            );
                        }
                    }
                }
                if (!empty($insert_data)) {
                    if (Region::insert($insert_data)) {
//                        $updatedConf = var_export($conf, 1);
//                        if (\File::put(config_path() . '/app.php', "<?php\n return $updatedConf ;")) {
                            return true;
//                        }
                    }
                }
            }
        }
        return false;
    }

//    public function create(array $input) {
//        $tagsArray = $this->createTags($input['tags']);
//        $categoriesArray = $this->createCategories($input['categories']);
//        unset($input['tags'], $input['categories']);
//
//        DB::transaction(function () use ($input, $tagsArray, $categoriesArray) {
//            $input['slug'] = str_slug($input['name']);
//            $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
//            $input = $this->uploadImage($input);
//            $input['created_by'] = access()->user()->id;
//
//            if ($region = Region::create($input)) {
//                // Inserting associated category's id in mapper table
//                if (count($categoriesArray)) {
//                    $region->categories()->sync($categoriesArray);
//                }
//
//                // Inserting associated tag's id in mapper table
//                if (count($tagsArray)) {
//                    $region->tags()->sync($tagsArray);
//                }
//
//                event(new RegionCreated($region));
//
//                return true;
//            }
//
//            throw new GeneralException(trans('exceptions.backend.regions.create_error'));
//        });
//    }

    /**
     * Update Region.
     *
     * @param \App\Models\Regions\Region $region
     * @param array                  $input
     */
    public function update(Region $region, array $input) {
        $tagsArray = $this->createTags($input['tags']);
        $categoriesArray = $this->createCategories($input['categories']);
        unset($input['tags'], $input['categories']);

        $input['slug'] = str_slug($input['name']);
        $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
        $input['updated_by'] = access()->user()->id;

        // Uploading Image
        if (array_key_exists('featured_image', $input)) {
            $this->deleteOldFile($region);
            $input = $this->uploadImage($input);
        }

        DB::transaction(function () use ($region, $input, $tagsArray, $categoriesArray) {
            if ($region->update($input)) {

                // Updateing associated category's id in mapper table
                if (count($categoriesArray)) {
                    $region->categories()->sync($categoriesArray);
                }

                // Updating associated tag's id in mapper table
                if (count($tagsArray)) {
                    $region->tags()->sync($tagsArray);
                }

                event(new RegionUpdated($region));

                return true;
            }

            throw new GeneralException(
            trans('exceptions.backend.regions.update_error')
            );
        });
    }

    /**
     * Creating Tags.
     *
     * @param array $tags
     *
     * @return array
     */
    public function createTags($tags) {
        //Creating a new array for tags (newly created)
        $tags_array = [];

        foreach ($tags as $tag) {
            if (is_numeric($tag)) {
                $tags_array[] = $tag;
            } else {
                $newTag = RegionTag::create(['name' => $tag, 'status' => 1, 'created_by' => 1]);
                $tags_array[] = $newTag->id;
            }
        }

        return $tags_array;
    }

    /**
     * Creating Categories.
     *
     * @param Array($categories)
     *
     * @return array
     */
    public function createCategories($categories) {
        //Creating a new array for categories (newly created)
        $categories_array = [];

        foreach ($categories as $category) {
            if (is_numeric($category)) {
                $categories_array[] = $category;
            } else {
                $newCategory = RegionCategory::create(['name' => $category, 'status' => 1, 'created_by' => 1]);

                $categories_array[] = $newCategory->id;
            }
        }

        return $categories_array;
    }

    /**
     * @param \App\Models\Regions\Region $region
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Region $region) {
        DB::transaction(function () use ($region) {
            if ($region->delete()) {
                RegionMapCategory::where('region_id', $region->id)->delete();
                RegionMapTag::where('region_id', $region->id)->delete();

                event(new RegionDeleted($region));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.regions.delete_error'));
        });
    }

    /**
     * Upload Image.
     *
     * @param array $input
     *
     * @return array $input
     */
    public function uploadImage($input) {
        $avatar = $input['featured_image'];

        if (isset($input['featured_image']) && !empty($input['featured_image'])) {
            $fileName = time() . $avatar->getRegionOriginalName();

            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()),'public');

            $input = array_merge($input, ['featured_image' => $fileName]);

            return $input;
        }
    }

    /**
     * Destroy Old Image.
     *
     * @param int $id
     */
    public function deleteOldFile($model) {
        $fileName = $model->featured_image;

        return $this->storage->delete($this->upload_path . $fileName);
    }

}
