<?php

namespace App\Repositories\Backend\Infographic;

use App\Models\Infographic\Infographic;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Events\Backend\Infographic\InfographicCreated;
use App\Events\Backend\Infographic\InfographicDeleted;
use App\Events\Backend\Infographic\InfographicUpdated;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class InfographicRepository.
 */
class InfographicRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Infographic::class;

    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct() {
//        $this->upload_path = 'img' . DIRECTORY_SEPARATOR . 'infographics' . DIRECTORY_SEPARATOR;
//        $this->storage = Storage::disk('do');

        $this->upload_path = '/static/assets/img' . DIRECTORY_SEPARATOR . 'infographics' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('do');
    }

    public function getForDataTable() {
        return $this->query()
                        ->leftjoin(config('access.users_table'), config('access.users_table') . '.id', '=', config('module.infographics.table') . '.created_by')
                        ->where(config('module.infographics.table') . '.report_id', '=', $_POST["report_id"])
                        ->select([
                            config('module.infographics.table') . '.id',
                            config('module.infographics.table') . '.name',
                            config('module.infographics.table') . '.status',
                            config('module.infographics.table') . '.created_by',
                            config('module.infographics.table') . '.created_at',
                            config('access.users_table') . '.first_name as user_name',
        ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input) {
        DB::transaction(function () use ($input) {
            $input['slug'] = str_slug($input['name']);
            $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
            $input = $this->uploadImage($input);
            $input['created_by'] = access()->user()->id;
            if ($infographic = Infographic::create($input)) {

                event(new InfographicCreated($infographic));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.infographics.create_error'));
        });
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Infographic $infographic
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Infographic $infographic, array $input) {
        $input['slug'] = str_slug($input['name']);
        $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
        $input['updated_by'] = access()->user()->id;

        // Uploading Image
        if (array_key_exists('featured_image', $input)) {
            $this->deleteOldFile($infographic);
            $input = $this->uploadImage($input);
        }

        DB::transaction(function () use ($infographic, $input) {
            if ($infographic->update($input)) {

                event(new InfographicUpdated($infographic));

                return true;
            }

            throw new GeneralException(
            trans('exceptions.backend.infographics.update_error')
            );
        });
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Infographic $infographic
     * @throws GeneralException
     * @return bool
     */
    public function delete(Infographic $infographic) {
        if ($infographic->delete()) {
//            event(new InfographicDeleted($infographic));
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.infographics.delete_error'));
    }

    public function uploadImage($input) {
        $avatar = $input['featured_image'];

        if (isset($input['featured_image']) && !empty($input['featured_image'])) {
            $fileName = time() . $avatar->getClientOriginalName();

            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()), 'public');

            $input = array_merge($input, ['featured_image' => $fileName]);

            return $input;
        }
    }

    /**
     * Destroy Old Image.
     *
     * @param int $id
     */
    public function deleteOldFile($model) {
        $fileName = $model->featured_image;

        return $this->storage->delete($this->upload_path . $fileName);
    }

}
