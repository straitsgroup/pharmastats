<?php

namespace App\Repositories\Backend\Pressreleases;

use App\Events\Backend\Pressreleases\PressreleaseCreated;
use App\Events\Backend\Pressreleases\PressreleaseDeleted;
use App\Events\Backend\Pressreleases\PressreleaseUpdated;
use App\Exceptions\GeneralException;
use App\Models\Pressrelease\Pressrelease;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class PressreleasesRepository.
 */
class PressreleasesRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Pressrelease::class;

    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct() {
        $this->upload_path = '/static/assets/img' . DIRECTORY_SEPARATOR . 'pressrelease' . DIRECTORY_SEPARATOR;
//        $this->upload_path = 'img' . DIRECTORY_SEPARATOR . 'pressrelease' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('do');
    }

    /**
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->leftjoin(config('access.users_table'), config('access.users_table') . '.id', '=', config('module.pressreleases.table') . '.created_by')
                        ->select([
                            config('module.pressreleases.table') . '.id',
                            config('module.pressreleases.table') . '.name',
                            config('module.pressreleases.table') . '.publish_datetime',
                            config('module.pressreleases.table') . '.status',
                            config('module.pressreleases.table') . '.created_by',
                            config('module.pressreleases.table') . '.created_at',
                            config('access.users_table') . '.first_name as user_name',
        ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input) {


        if ($this->query()->where('name', $input['name'])->first()) {
            throw new GeneralException(trans('exceptions.backend.pressreleases.already_exists'));
        }

        DB::transaction(function () use ($input) {
            $input['slug'] = str_slug($input['name']);
            $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
            $input = $this->uploadImage($input);
            $input['created_by'] = access()->user()->id;

            if ($pressrelease = Pressrelease::create($input)) {

//                event(new PressreleaseCreated($pressrelease));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.pressreleases.create_error'));
        });
    }

    /**
     * Update Pressrelease.
     *
     * @param \App\Models\Pressreleases\Pressrelease $pressrelease
     * @param array                  $input
     */
    public function update(Pressrelease $pressrelease, array $input) {

        if ($this->query()->where('name', $input['name'])->where('id', '!=', $pressrelease->id)->first()) {
            throw new GeneralException(trans('exceptions.backend.pressreleases.already_exists'));
        }
        $input['slug'] = str_slug($input['name']);
        $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
        $input['updated_by'] = access()->user()->id;

        // Uploading Image
        if (array_key_exists('featured_image', $input)) {
            $this->deleteOldFile($pressrelease);
            $input = $this->uploadImage($input);
        }

        DB::transaction(function () use ($pressrelease, $input) {
            if ($pressrelease->update($input)) {

//                event(new PressreleaseUpdated($pressrelease));

                return true;
            }

            throw new GeneralException(
            trans('exceptions.backend.pressreleases.update_error')
            );
        });
    }

    /**
     * Creating Tags.
     *
     * @param array $tags
     *
     * @return array
     */
    public function createTags($tags) {
        //Creating a new array for tags (newly created)
        $tags_array = [];

        foreach ($tags as $tag) {
            if (is_numeric($tag)) {
                $tags_array[] = $tag;
            } else {
                $newTag = PressreleaseTag::create(['name' => $tag, 'status' => 1, 'created_by' => 1]);
                $tags_array[] = $newTag->id;
            }
        }

        return $tags_array;
    }

    /**
     * Creating Categories.
     *
     * @param Array($categories)
     *
     * @return array
     */
    public function createCategories($categories) {
        //Creating a new array for categories (newly created)
        $categories_array = [];

        foreach ($categories as $category) {
            if (is_numeric($category)) {
                $categories_array[] = $category;
            } else {
                $newCategory = PressreleaseCategory::create(['name' => $category, 'status' => 1, 'created_by' => 1]);

                $categories_array[] = $newCategory->id;
            }
        }

        return $categories_array;
    }

    /**
     * @param \App\Models\Pressreleases\Pressrelease $pressrelease
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Pressrelease $pressrelease) {
        DB::transaction(function () use ($pressrelease) {
            if ($pressrelease->delete()) {

//                event(new PressreleaseDeleted($pressrelease));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.pressreleases.delete_error'));
        });
    }

    /**
     * Upload Image.
     *
     * @param array $input
     *
     * @return array $input
     */
    public function uploadImage($input) {
        $avatar = $input['featured_image'];

        if (isset($input['featured_image']) && !empty($input['featured_image'])) {
            $fileName = time() . $avatar->getClientOriginalName();

            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()), 'public');

            $input = array_merge($input, ['featured_image' => $fileName]);

            return $input;
        }
    }

    /**
     * Destroy Old Image.
     *
     * @param int $id
     */
    public function deleteOldFile($model) {
        $fileName = $model->featured_image;

        return $this->storage->delete($this->upload_path . $fileName);
    }

}
