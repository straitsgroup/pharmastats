<?php

use App\Helpers\uuid;
use App\Models\Notification\Notification;
use App\Models\Settings\Setting;
use Carbon\Carbon as Carbon;

/**
 * Henerate UUID.
 *
 * @return uuid
 */
function generateUuid() {
    return uuid::uuid4();
}

if (!function_exists('homeRoute')) {

    /**
     * Return the route to the "home" page depending on authentication/authorization status.
     *
     * @return string
     */
    function homeRoute() {
        if (access()->allow('view-backend')) {
            return 'admin.dashboard';
        } elseif (auth()->check()) {
            return 'frontend.user.dashboard';
        }

        return 'frontend.index';
    }

}

/*
 * Global helpers file with misc functions.
 */
if (!function_exists('app_name')) {

    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name() {
        return config('app.name');
    }

}

if (!function_exists('access')) {

    /**
     * Access (lol) the Access:: facade as a simple function.
     */
    function access() {
        return app('access');
    }

}

if (!function_exists('history')) {

    /**
     * Access the history facade anywhere.
     */
    function history() {
        return app('history');
    }

}

if (!function_exists('gravatar')) {

    /**
     * Access the gravatar helper.
     */
    function gravatar() {
        return app('gravatar');
    }

}

if (!function_exists('includeRouteFiles')) {

    /**
     * Loops through a folder and requires all PHP files
     * Searches sub-directories as well.
     *
     * @param $folder
     */
    function includeRouteFiles($folder) {
        $directory = $folder;
        $handle = opendir($directory);
        $directory_list = [$directory];

        while (false !== ($filename = readdir($handle))) {
            if ($filename != '.' && $filename != '..' && is_dir($directory . $filename)) {
                array_push($directory_list, $directory . $filename . '/');
            }
        }

        foreach ($directory_list as $directory) {
            foreach (glob($directory . '*.php') as $filename) {
                require $filename;
            }
        }
    }

}

if (!function_exists('getRtlCss')) {

    /**
     * The path being passed is generated by Laravel Mix manifest file
     * The webpack plugin takes the css filenames and appends rtl before the .css extension
     * So we take the original and place that in and send back the path.
     *
     * @param $path
     *
     * @return string
     */
    function getRtlCss($path) {
        $path = explode('/', $path);
        $filename = end($path);
        array_pop($path);
        $filename = rtrim($filename, '.css');

        return implode('/', $path) . '/' . $filename . '.rtl.css';
    }

}

if (!function_exists('settings')) {

    /**
     * Access the settings helper.
     */
    function settings() {
        // Settings Details
        $settings = Setting::latest()->first();
        if (!empty($settings)) {
            return $settings;
        }
    }

}

if (!function_exists('createNotification')) {

    /**
     * create new notification.
     *
     * @param  $message    message you want to show in notification
     * @param  $userId     To Whom You Want To send Notification
     *
     * @return object
     */
    function createNotification($message, $userId) {
        $notification = new Notification();

        return $notification->insert([
                    'message' => $message,
                    'user_id' => $userId,
                    'type' => 1,
                    'created_at' => Carbon::now(),
        ]);
    }

}

if (!function_exists('escapeSlashes')) {

    /**
     * Access the escapeSlashes helper.
     */
    function escapeSlashes($path) {
        $path = str_replace('\\', DIRECTORY_SEPARATOR, $path);
        $path = str_replace('//', DIRECTORY_SEPARATOR, $path);
        $path = trim($path, DIRECTORY_SEPARATOR);

        return $path;
    }

}

if (!function_exists('getMenuItems')) {

    /**
     * Converts items (json string) to array and return array.
     */
    function getMenuItems($type = 'backend', $id = null) {
        $menu = new \App\Models\Menu\Menu();
        $menu = $menu->where('type', $type);
        if (!empty($id)) {
            $menu = $menu->where('id', $id);
        }
        $menu = $menu->first();
        if (!empty($menu) && !empty($menu->items)) {
            return json_decode($menu->items);
        }

        return [];
    }

}

if (!function_exists('getRouteUrl')) {

    /**
     * Converts querystring params to array and use it as route params and returns URL.
     */
    function getRouteUrl($url, $url_type = 'route', $separator = '?') {
        $routeUrl = '';
        if (!empty($url)) {
            if ($url_type == 'route') {
                if (strpos($url, $separator) !== false) {
                    $urlArray = explode($separator, $url);
                    $url = $urlArray[0];
                    parse_str($urlArray[1], $params);
                    $routeUrl = route($url, $params);
                } else {
                    $routeUrl = route($url);
                }
            } else {
                $routeUrl = $url;
            }
        }

        return $routeUrl;
    }

}

if (!function_exists('renderMenuItems')) {

    /**
     * render sidebar menu items after permission check.
     */
    function renderMenuItems($items, $viewName = 'backend.includes.partials.sidebar-item') {
        foreach ($items as $item) {
            // if(!empty($item->url) && !Route::has($item->url)) {
            //     return;
            // }
            if (!empty($item->view_permission_id)) {
                if (access()->allow($item->view_permission_id)) {
                    echo view($viewName, compact('item'));
                }
            } else {
                echo view($viewName, compact('item'));
            }
        }
    }

}

if (!function_exists('isActiveMenuItem')) {

    /**
     * checks if current URL is of current menu/sub-menu.
     */
    function isActiveMenuItem($item, $separator = '?') {
        $item->clean_url = $item->url;
        if (strpos($item->url, $separator) !== false) {
            $item->clean_url = explode($separator, $item->url, -1);
        }
        if (Active::checkRoutePattern($item->clean_url)) {
            return true;
        }
        if (!empty($item->children)) {
            foreach ($item->children as $child) {
                $child->clean_url = $child->url;
                if (strpos($child->url, $separator) !== false) {
                    $child->clean_url = explode($separator, $child->url, -1);
                }
                if (Active::checkRoutePattern($child->clean_url)) {
                    return true;
                }
            }
        }

        return false;
    }

}

if (!function_exists('checkDatabaseConnection')) {

    /**
     * @return bool
     */
    function checkDatabaseConnection() {
        try {
            DB::connection()->reconnect();

            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

}
//-----------Custom Report Form Creation----------------------------

if (!function_exists('isActiveCustomReportItem')) {

    function isActiveCustomReportItem($item, $separator = '?') {
        $item->clean_url = $item->label_type;
        if (strpos($item->label_type, $separator) !== false) {
            $item->clean_url = explode($separator, $item->label_type, -1);
        }
        if (Active::checkRoutePattern($item->clean_url)) {
            return true;
        }
        if (!empty($item->children)) {
            foreach ($item->children as $child) {
                $child->clean_url = $child->label_type;
                if (strpos($child->label_type, $separator) !== false) {
                    $child->clean_url = explode($separator, $child->label_type, -1);
                }
                if (Active::checkRoutePattern($child->clean_url)) {
                    return true;
                }
            }
        }

        return false;
    }

}

if (!function_exists('renderCustomReportItems')) {



    function renderCustomReportItems($items, $reports = "", $parent = "", $chartsdata, $viewName = 'backend.includes.partials.customreport-item', $reportId) {
//        dd($items);
        foreach ($items as $item) {
            $item->name1 = $item->name;
            if ($parent != "") {
                $item->name = $parent . "!" . $item->name;
            }
            $iname = str_replace(" ", "_", $item->name);
            if (isset($reports->$iname)) {
                $item->value = $reports->$iname;
            } else {
                $item->value = "";
            }
            if (isset($item->segment_type) && !empty($item->segment_type)) {
                $chartsdata = getSegmentalOtherCharts($item->segment_type, $reportId);
            }
            echo view($viewName, compact('item', 'reports', 'chartsdata', 'reportId'));
        }
    }

}

function getSegmentalOtherCharts($segment, $reportId) {
    $chartData = DB::table('custom_reportcharts')
                    ->whereIn("type", [1])
                    ->where('report_id', $reportId)
                    ->where('segmental_type', $segment)
                    ->get()->toArray();

    $data[''] = "Select Title for Table and Chart";
    if (isset($chartData)) {
        foreach ($chartData as $index => $chart) {
            $tables = json_decode($chart->data);
            foreach ($tables as $key => $table) {
                foreach ($table as $key1 => $table1) {
                    foreach ($table1 as $key2 => $table2) {
                        foreach ($table2 as $key3 => $table3) {
                            if (isset($table3->Title)) {
                                $citytitle = $table3->Title;
                                $data[$index . "/" . $key . "/" . $key1 . "/" . $key2 . "/" . $key3] = $citytitle;
                                break;
                            }

                            if ($key3 == 'Title') {
                                $tit = $table3;
                                $data[$index . "/" . $key . "/" . $key1 . "/" . $key2 . "/" . $chartData[$index]->id] = $tit;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    return $data;
}

if (!function_exists('getCustomReportItems')) {

    function getCustomReportItems($id = null, $region_id = 1) {
        $customreport = new \App\Models\CustomReport\CustomReport();
        if (!empty($id)) {
            $customreport = $customreport->where('report_id', $id)
                    ->where('region_id', $region_id)
                    ->where('client_profile_id', 0);
        }
        $customreport = $customreport->first();
        if (!empty($customreport) && !empty($customreport->items)) {
            return json_decode($customreport->items);
        }

        return [];
    }

}


//***************************
//client form render
if (!function_exists('renderCustomReportItems1')) {

    function renderCustomReportItems1($items, $reports = "", $parent = "", $chartsdata = '', $viewName = 'backend.includes.partials.customreport-client-item') {
//        dd($items);
        foreach ($items as $item) {
//            dd($item);
            $item->name1 = $item->name;
            if ($parent != "") {
                $item->slug = $parent . "-" . $item->slug;
            }
            $iname = str_replace(" ", "_", $item->slug);
            if (isset($reports->$iname)) {
                $item->value = $reports->$iname;
            } else {
                $item->value = "";
            }
            echo view($viewName, compact('item', 'reports', 'chartsdata'));
        }
    }

}

if (!function_exists('getCustomReportItems1')) {

    function getCustomReportItems1($id = null, $form_type = 1, $client_id = 0, $region_id=1) {
        $customreport = new \App\Models\CustomReport\CustomReport();
        if (!empty($client_id)) {
            $customreport = $customreport->where('report_id', $id)
                    ->where('form_type', $form_type)
                    ->where('region_id', $region_id)
                    ->where('client_profile_id', $client_id);
        }
        $customreport = $customreport->first();
        if (!empty($customreport) && !empty($customreport->items)) {
            return json_decode($customreport->items);
        }

        return [];
    }

}


//-------------------Report Menu------------
if (!function_exists('isActiveReportMenuItem')) {

    function isActiveReportMenuItem($item, $separator = '?') {
        $item->clean_url = $item->label_type;
        if (strpos($item->label_type, $separator) !== false) {
            $item->clean_url = explode($separator, $item->label_type, -1);
        }
        if (Active::checkRoutePattern($item->clean_url)) {
            return true;
        }
        if (!empty($item->children)) {
            foreach ($item->children as $child) {
                $child->clean_url = $child->label_type;
                if (strpos($child->label_type, $separator) !== false) {
                    $child->clean_url = explode($separator, $child->label_type, -1);
                }
                if (Active::checkRoutePattern($child->clean_url)) {
                    return true;
                }
            }
        }

        return false;
    }

}


if (!function_exists('renderReportMenuItems')) {


    function renderReportMenuItems($items, $report_id = '', $parent = "", $perm = [], $viewName = 'backend.includes.partials.reportmenu-item') {
        foreach ($items as $item) {

            $item->name1 = $item->name;
            if ($parent != "") {
                $item->name1 = $parent . "!" . $item->name;
            }
            if (isset($item->access) && in_array($item->access, $perm)) {
                echo view($viewName, compact('item', 'report_id', 'perm'));
            }
        }
    }

}

if (!function_exists('getReportMenuItems')) {

    function getReportMenuItems($id = null) {
        $reportmenu = new \App\Models\CustomReport\CustomReport();
        if (!empty($id)) {
            $reportmenu = $reportmenu->where('report_id', $id);
        }
        $reportmenu = $reportmenu->first();
        if (!empty($reportmenu) && !empty($reportmenu->items)) {
            return json_decode($reportmenu->items);
        }

        return [];
    }

}

//-------------------Report Index------------
if (!function_exists('isActiveReportIndexItem')) {

    function isActiveReportIndexItem($item, $separator = '?') {
        $item->clean_url = $item->label_type;
        if (strpos($item->label_type, $separator) !== false) {
            $item->clean_url = explode($separator, $item->label_type, -1);
        }
        if (Active::checkRoutePattern($item->clean_url)) {
            return true;
        }
        if (!empty($item->children)) {
            foreach ($item->children as $child) {
                $child->clean_url = $child->label_type;
                if (strpos($child->label_type, $separator) !== false) {
                    $child->clean_url = explode($separator, $child->label_type, -1);
                }
                if (Active::checkRoutePattern($child->clean_url)) {
                    return true;
                }
            }
        }

        return false;
    }

}


if (!function_exists('renderReportIndexItems')) {


    function renderReportIndexItems($items, $viewName = 'backend.includes.partials.reportindex-item') {
        foreach ($items as $item) {
            echo view($viewName, compact('item'));
        }
    }

}

if (!function_exists('getReportIndexItems')) {

    function getReportIndexItems($id = null) {
        $reportindex = new \App\Models\CustomReport\CustomReport();
        if (!empty($id)) {
            $reportindex = $reportindex->where('report_id', $id);
        }
        $reportindex = $reportindex->first();
        if (!empty($reportindex) && !empty($reportindex->items)) {
            return json_decode($reportindex->items);
        }

        return [];
    }

}

if (!function_exists('isActiveReportIndexItem1')) {

    function isActiveReportIndexItem1($item, $separator = '?') {
        $item->clean_url = $item->label_type;
        if (strpos($item->label_type, $separator) !== false) {
            $item->clean_url = explode($separator, $item->label_type, -1);
        }
        if (Active::checkRoutePattern($item->clean_url)) {
            return true;
        }
        if (!empty($item->children)) {
            foreach ($item->children as $child) {
                $child->clean_url = $child->label_type;
                if (strpos($child->label_type, $separator) !== false) {
                    $child->clean_url = explode($separator, $child->label_type, -1);
                }
                if (Active::checkRoutePattern($child->clean_url)) {
                    return true;
                }
            }
        }

        return false;
    }

}

if (!function_exists('renderReportIndexItems1')) {


    function renderReportIndexItems1($items, $report_id = "", $report = "", $parent = "", $viewName = 'backend.includes.partials.reportindex-item1') {
        foreach ($items as $item) {
            $item->name1 = $item->name;
            if ($item->name == 'Research Methodology1') {
                $item->name1 = 'Research Methodology';
            }
            if ($parent != "") {
                $item->name = $parent . "!" . $item->name;
                $item->name = str_replace(" ", "-", $item->name);
            }


            echo view($viewName, compact('item', 'report_id', 'report'));
        }
    }

}

if (!function_exists('getReportIndexItems1')) {

    function getReportIndexItems1($id = null) {
        $reportindex = new \App\Models\CustomReport\CustomReport();
        if (!empty($id)) {
            $reportindex = $reportindex->where('report_id', $id);
        }
        $reportindex = $reportindex->first();
        if (!empty($reportindex) && !empty($reportindex->items)) {
            return json_decode($reportindex->items);
        }

        return [];
    }

}

if (!function_exists('getHeader')) {

    function getHeader() {
        $header = new \App\Models\Settings\Setting();
        $header = $header->first();
        return ($header->google_analytics);
    }

}
